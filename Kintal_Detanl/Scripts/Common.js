﻿var loader = "<div class='ts-lom'><div class='ts-loOl'></div><div class='ts-loBg'></div></div>";
//var loader = "";
function AddLoader() {
  $(".addLoader").append(loader);
}

function RemoveLoader() {
  $(".addLoader").find('.ts-lom').remove();
}

$(document).ajaxStart(function () {
  AddLoader();
}).ajaxStop(function () {
  RemoveLoader();
});

/// Function to Open Link in Browser Popup Window
// url : from view (@Url.Action("Index", "Home"))
// width : 1100 (int)
// height: 700 (int)
function OpenBrowserPopupWindow(url, width, height) {
  window.open(url, '', 'width=' + width + ',height=' + height + ',scrollbars=yes,menubar=yes,status=yes,resizable=yes,directories=false,location=false,left=0,top=0');
}

function OpenPage(url, crm_menuId) {
  try {
    if (crm_menuId != undefined && crm_menuId != null) {
      localStorage.setItem('_crm_menuId', crm_menuId); //set
    }
    else {
      localStorage.setItem('_crm_menuId', ""); //set
    }
  } catch (e) { }

  location.href = url;
}

// Function to Convert Json Date to Date
function ConvertJsonDate(date) {
  if (date != '' && date != null) {
    var parsedDate = new Date(parseInt(date.substr(6)));
    var jsDate = new Date(parsedDate);
    var day = jsDate.getDate();
    var month = (jsDate.getMonth() + 1);
    if (parseInt(day) < 10) {
      day = '0' + day;
    }

    if (parseInt(month) < 10) {
      month = '0' + month;
    }
    var convertedDate = day + '/' + month + '/' + jsDate.getFullYear();
    return convertedDate;
  }
  else {
    return '';
  }
}

// function to Close Current Window
function CloseWindow() {
  window.close();
}

// Debtor Search Popup
function OpenDebtorSearch() {
  $("#searchDebtor").val('');
  $("#filterMode").val('ALL');
  $("#selectMode").val('0');
  SearchDebtor();
  $("#gridDebtor").data("kendoGrid").dataSource.page(1);
  setTimeout(function () { $("#debtorModel").modal('show'); }, 250);
}

// Open Staff Search Popup
// controlId field is for control which is in need to bind for in hidden field of Main Module
// controlNameId field is for control which is in need to bind for in textbox of Main Module
function OpenStaffSearch(controlId, controlNameId) {
  $("#selectModeStaff").val('0');
  $("#searchStaff").val('');
  $("#gridStaff").data("kendoGrid").dataSource.data([]);
  $("#gridStaff").data("kendoGrid").dataSource.page(1);
  if (controlId != undefined && controlId != null) {
    $("#hdnStaffControlId").val(controlId);
    $("#hdnStaffControlNameId").val(controlNameId);
  }
  setTimeout(function () { $("#staffModel").modal('show'); }, 200);
}

// Open Email Search Popup
function OpenEmailSearch(OppoId, TemplateIds) {
  if (OppoId == undefined) {
    jAlert("Please save Opportunity Details first");
    return false;
  }
  else if ($("#AssignedTo").val() == '' || $("#AssignedTo").val() == 0) {
    jAlert("Please add Assigned To details");
    return false;
  }

  if (TemplateIds != undefined && TemplateIds != null) {
    $("#hdnTemplateIds").val(TemplateIds);
  }

  SearchEmailTemplates();

  setTimeout(function () { $("#emailModel").modal('show'); }, 200);
}

// Open Contact Search Popup
function OpenContactSearch(contactIdField, contactNameField) {
  $("#selectModeContact").val('0');
  $("#filterModeContact").val('ALL');
  $("#searchContact").val('');
  $("#gridContact").data("kendoGrid").dataSource.data([]);
  $("#gridContact").data("kendoGrid").dataSource.page(1);
  if (contactIdField != undefined && contactIdField != null) {
    $("#hdnContactIdField").val(contactIdField);
    $("#hdnContactNameField").val(contactNameField);
  }

  setTimeout(function () { $("#contactModel").modal('show'); }, 200);
}

// Open Opportunity Stage Search
function OpenOppoStageSearch() {
  $("#selectModeStage").val('0');
  $("#searchOppoStage").val('');
  $("#gridStage").data("kendoGrid").dataSource.data([]);
  $("#gridStage").data("kendoGrid").dataSource.page(1);
  setTimeout(function () { $("#oppoStageModel").modal('show'); }, 200);
}

// Function to set Date Format for Kendo Grid Filter Control
function DateFormat(element) {
  element.kendoDatePicker({
    format: "{0:dd/MM/yyyy}"
  });
}

// Company Search Popup
function OpenCompanySearch(type) {
  $("#searchCompany").val('');
  $("#filterMode").val('ALL');
  $("#filterType").val('DEBTOR');
  $("#selectMode").val('0');
  $("#hdnType").val(type);
  SearchCompany();
  $("#gridDebtor").data("kendoGrid").dataSource.page(1);
  $("#gridCreditor").data("kendoGrid").dataSource.page(1);
  $("#gridProspect").data("kendoGrid").dataSource.page(1);
  setTimeout(function () { $("#companyModel").modal('show'); }, 250);
}


function FindValueFromJSONString(obj, prop) {
  prop = prop.split('.');
  for (var i = 0; i < prop.length; i++) {
    if (typeof obj[prop[i]] == 'undefined') {
      return null;
    }
    else {
      obj = obj[prop[i]];
    }
  }
  return obj;
}

// Creditor Search Popup
function OpenCreditorSearch() {

  $("#searchCreditor").val('');
  $("#filterMode").val('ALL');
  $("#selectMode").val('0');
  SearchCreditor();
  $("#gridCreditor").data("kendoGrid").dataSource.page(1);

  setTimeout(function () { $("#creditorModel").modal('show'); }, 250);
}

function StrNullOrEmpty(inp) {
  return ((inp == null || !inp || $.trim(inp) == ""));
}

function MspOpenMailTo(to, cc, bcc, subject, body) {
  //Body text present will turn off outlook signature
  var mailtoTxt = 'mailto:';
  var queryStr = '';
  var ampersan = '';
  if (!StrNullOrEmpty(to)) {
    mailtoTxt = mailtoTxt + to.replace('&', '');
  }
  if (!StrNullOrEmpty(cc)) {
    ampersan = (!StrNullOrEmpty(queryStr)) ? '&' : '';
    queryStr = queryStr + ampersan + 'cc=' + cc.replace('&', '');
  }
  if (!StrNullOrEmpty(bcc)) {
    ampersan = (!StrNullOrEmpty(queryStr)) ? '&' : '';
    queryStr = queryStr + ampersan + 'bcc=' + bcc.replace('&', '');
  }
  if (!StrNullOrEmpty(subject)) {
    ampersan = (!StrNullOrEmpty(queryStr)) ? '&' : '';
    queryStr = queryStr + ampersan + 'subject=' + subject.replace('&', '');
  }
  if (!StrNullOrEmpty(body)) {
    ampersan = (!StrNullOrEmpty(queryStr)) ? '&' : '';
    queryStr = queryStr + ampersan + 'body=' + body.replace('&', '');
  }
  mailtoTxt = (!StrNullOrEmpty(queryStr)) ? mailtoTxt + '?' + queryStr : mailtoTxt;
  window.location = mailtoTxt;
  return mailtoTxt;
}

function OpenPageForEdit(url, idControl, querystringParam, message) {
  if ($("#" + idControl).val() != null && $("#" + idControl).val().toString().length > 0) {
    if (querystringParam.toString().toUpperCase() == 'ID') {
      OpenBrowserPopupWindow(url + "/" + $("#" + idControl).val(), 1400, 800);
    }
    else {
      OpenBrowserPopupWindow(url + "?" + querystringParam + "=" + $("#" + idControl).val(), 1400, 800);
    }
  }
  else {
    if (message != undefined && message != null && message.toString().length > 0) {
      jAlert(message, "Information Dialog");
    }
  }
}

function SetControlTextAndShowHideControlsUsingScreenSetting(jsonString) {
  jsonString = JSON.parse(jsonString);
  if (jsonString != null && jsonString != "") {
    try {
      var commaSeperatedKeys = GetNthLevelKeys(jsonString, 1);
      if (commaSeperatedKeys != undefined && commaSeperatedKeys != null) {
        var keysArray = commaSeperatedKeys.toString().split(",");
        var isRequired = false;
        if (keysArray != undefined && keysArray != null && keysArray.length > 0) {
          for (i = 0; i < keysArray.length; i++) {
            var key = keysArray[i];
            if (jsonString[key] != undefined && jsonString[key] != null && jsonString[key].toString().length > 0) {              
              if (key.indexOf("Text") >= 0) {
                var controlId = key.replace('Text', '');
                $("#" + controlId).html(jsonString[key]);
                $("#" + controlId).text(jsonString[key]);
              }
              else if (key.indexOf("Visible") >= 0) {
                if (jsonString[key] != undefined && jsonString[key] != null && jsonString[key].toString().toUpperCase() == 'FALSE') {
                  var controlId = key.replace('Visible', '');
                  $("#" + controlId).hide();
                }
                else {
                  var controlId = key.replace('Visible', '');
                  $("#" + controlId).show();
                }
              }
              else if (key.indexOf("Required") >= 0) {
                var splitKey = key.split('_');
                var asteriskLabelId = splitKey[0];
                var controlId = splitKey[1];
                if (splitKey.length > 2) {
                  for (var j = 2; j < splitKey.length; j++) {
                    controlId = controlId + '_' + splitKey[j];
                  }
                }

                asteriskLabelId = asteriskLabelId.replace('Required', '');
                if (jsonString[key] != undefined && jsonString[key] != null && jsonString[key].toString().toUpperCase() == 'REQUIRED') {
                  $("#" + controlId).attr("data-val-required", jsonString[asteriskLabelId + 'Message']);
                  isRequired = true;
                }
                else {
                  $("#" + asteriskLabelId + 'Required').text(' ');
                  $('#' + controlId).removeAttr('data-val');
                  $('#' + controlId).removeAttr('data-val-required');
                }
              }
              else if (key.indexOf("ClassName") >= 0) {

                var keyValue = jsonString[key].toString();
                var keyValueArray = keyValue.split(',');

                var controlId = key.replace('ClassName', '');
                if (keyValueArray != undefined && keyValueArray != null) {
                  var removeClass = keyValueArray[0];
                  $("#" + controlId).removeClass(removeClass).addClass(keyValueArray[1]);
                }
              }
              else if (key.toString() == "PageTitle") {
                document.title = jsonString[key];
              }
              else if (key.indexOf("SelectedValue") >= 0) {
                var controlId = key.replace('SelectedValue', '');
                $("#" + controlId).val(jsonString[key]);
              }
            }
          }
        }

        if (isRequired == false) {
          $(".mandatory-field-text").hide();
        }
        else {
          $(".mandatory-field-text").show();
        }
      }
    } catch (err) {
      jAlert(err.toString(), 'Information Dialog');
    }
  }
}

//This function will return nth level keys.
function GetNthLevelKeys(json, level) {
  var keys = "";
  var currLvlKeys = Object.keys(json);
  level = level - 1;

  if (typeof json !== 'object' || json === null) {
    return [];
  }

  if (level > 0) {
    for (var i = 0; i < currLvlKeys.length; i++) {
      keys = keys.concat(GetNthLevelKeys(json[currLvlKeys[i]], level));
    }
  }

  if (level === 0) {
    return currLvlKeys;
  }

  if (level < 0) {
    throw new Error("Cannot access level " + level + " of this object");
  }

  return keys;
}