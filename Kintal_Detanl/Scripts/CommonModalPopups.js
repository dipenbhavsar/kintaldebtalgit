﻿// Open Staff Search
function OpenStaffSearch(controlId, controlNameId) {
  try {
    $("#hdnStaffControlId").val(controlId);
    $("#hdnStaffControlNameId").val(controlNameId);
    $("#hdnStaffRequestFrom").val("STAFF");
    $("#selectModeStaff").val('0');
    $("#searchStaff").val('');
    $("#gridStaff").data("kendoGrid").dataSource.data([]);
    $("#gridStaff").data("kendoGrid").dataSource.page(1);
    setTimeout(function () { $("#staffModel").modal('show'); }, 200);
  } catch (err) {
    jAlert(err, 'Information Dialog');
  }
}