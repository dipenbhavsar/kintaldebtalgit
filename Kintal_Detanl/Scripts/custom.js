$(document).ready(function() {
	//Prevent Page Reload on all # links
	$("a[href='#'], button").click(function(e) {
		e.preventDefault();
	});


	// submenu
	$("#mainNavigation > ul  li  a").click(function(e) {
		e.preventDefault();
		var $this = $(this).closest('li');
		if(!$this.hasClass('open')){
			$this.siblings('.open').removeClass('open').find("> ul").stop(true, true).slideUp(300);
			$this.addClass('open').find("> ul").stop(true, true).slideDown(300);
		}
		else
			$this.removeClass('open').find("> ul").stop(true, true).slideUp(300);		
	});


	// Main Navigation
	$('.nav-icon').click(function(e) {
		//e.preventDefault();
		////$("body").toggleClass('menu-open');
		//$(".sidebar").toggleClass('sidebartgl');
		//$("#parentMenuItem").addClass("open");
		//$("#subMenuitems").show();
		//$("#subMenuitems").find('li').show();

	    //try {
	    //    localStorage.setItem('_menustatus', "0"); //set
	    //} catch (e) { }

		//if ($('#tglrSpan').hasClass('mnopen')) {		    
		//    $('#tglrSpan').removeClass('glyphicon-chevron-left').removeClass('mnopen').addClass('glyphicon-chevron-right').addClass('mnclose');
		//    setTimeout(function () {
		//        $(".tglrdiv").css({ 'left': '11%' });
		//    }, 250);
		//    $("body").removeClass('menu-open');
		//    $(".tglrdiv").prop('title', 'Show Menu');
		//}
		//else {
		//    // open menu		    
		//    $('#tglrSpan').removeClass('glyphicon-chevron-right').removeClass('mnclose').addClass('glyphicon-chevron-left').addClass('mnopen');
		//    $(".tglrdiv").css({ 'left': '95%' });
		//    //$("body").addClass('menu-open');
		//    $(".tglrdiv").prop('title', 'Hide Menu');
	    //}

	    $(".tglrdiv").trigger('click');
	});

	$('.tglrdiv').click(function (e) {

	    //$("body").toggleClass('menu-open');
	    $("#parentMenuItem").addClass("open");
	    $("#subMenuitems").show();
	    $("#subMenuitems").find('li').show();
	    $(".sidebar").toggleClass('sidebartgl');


	    if ($('#tglrSpan').hasClass('mnopen')) {
	        try {
	            localStorage.setItem('_menustatus', "0"); //set
	        } catch (e) {}
	        $('#tglrSpan').removeClass('glyphicon-chevron-left').removeClass('mnopen').addClass('glyphicon-chevron-right').addClass('mnclose');
	        setTimeout(function () {
	            $(".tglrdiv").css({ 'left': '11%' });
	        }, 250);
	        
	        $("body").removeClass('menu-open');
	        $(".tglrdiv").prop('title', 'Show Menu');
	    }
	    else {
	        // open menu	
	        try {
	            localStorage.setItem('_menustatus', "1"); //set
	        } catch (e) { }
	        $('#tglrSpan').removeClass('glyphicon-chevron-right').removeClass('mnclose').addClass('glyphicon-chevron-left').addClass('mnopen');
	        $(".tglrdiv").css({ 'left': '95%' });
	        $("body").addClass('menu-open');
	        $(".tglrdiv").prop('title', 'Hide Menu');
	    }
	});
	$(".menu-overlay").click(function(e){
		e.preventDefault();
		$("body").removeClass('menu-open');
	});


	// CustomSelect
	//$(".custom-select").dropkick({
	//	mobile: true
	//});

	//Multi Select 
	$(".multi-select").multiselect();


	$(".languages, .multiselect-container").click(function(e){		
		e.stopImmediatePropagation();
	});

	// CustomCheckbox
	$(".custom-checkbox").iCheck();

	// CustomRadio
	$(".custom-radio").iCheck();


	// Datetime picker
	//$(".calendar-input").datetimepicker({
	//	format: 'DD/MM/YYYY'
	//});


	// expand collaps
	$(".expand-collaps").click(function(){
		var $yearOpen = false;
		var $this = $(this).closest('tr');
		if(!$this.hasClass('open')){
			$(".open").removeClass('open');
			$(".show-detail").removeClass('show-detail');			
			$this.addClass('open');
			$this.closest('table').find("tr").each(function(){
				var $tr = $(this);	
				if($tr.hasClass('year'))
					$yearOpen = false;

				if($yearOpen)
					$tr.addClass('show-detail');

				if($tr.hasClass('open'))
					$yearOpen = true;
			});
		}else{
			$(".open").removeClass('open');
			$(".show-detail").removeClass('show-detail');	
		}
	});	


	// Expand search panel
	$(".btn-advance-search").click(function(e){
		e.preventDefault();
		$(".advance-search-panel").slideToggle();
	});

	$(".btn-box > .btn").click(function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var $this = $(this).closest(".btn-box");
		if(!$this.hasClass('open')){
			$(".btn-box.open").removeClass('open');
			$this.addClass('open')
		}else{
			$this.removeClass('open');
		}
	});

	$(".save-form, .btn-menu").click(function(e) {
		e.stopImmediatePropagation();		
	});	

	$("body").click(function(e) {
		// $(".btn-box.open").removeClass('open');
	});


	// form accordian
	$(".accordian-btn").click(function(e) {
		e.preventDefault();
		var $box = $(this).closest('.accordian-block');
		$(this).toggleClass('closed');
		$box.find(".accordian-content").stop(true, true).slideToggle();
	});

	// tooltip
	$(".form-control").focus(function(){
		$(this).siblings('.tooltip-block').show();
	});
	$(".form-control").blur(function(){
		$(this).siblings('.tooltip-block').hide();
	});



	$(".dk-selected").focus(function(){
		$(this).closest('.dk-select').siblings('.tooltip-block').show();		
	});

	$(".dk-selected").blur(function(){
		$(this).closest('.dk-select').siblings('.tooltip-block').hide();		
	});

	
	$(".custom-checkbox").focus(function(){
		$(this).closest('.icheckbox').siblings('.tooltip-block').show();		
	});
	$(".custom-checkbox").blur(function(){
		$(this).closest('.icheckbox').siblings('.tooltip-block').hide();		
	});

	
	$(".custom-radio").focus(function(){
		$(this).closest('.iradio').siblings('.tooltip-block').show();		
	});
	$(".custom-radio").blur(function(){
		$(this).closest('.iradio').siblings('.tooltip-block').hide();		
	});

	

	// $('.form-control[data-toggle="tooltip"]').each(function(){
	// 	var $this = $(this);
	// 	var $title = $this.attr("data-title");
	// 	$this.tooltip({'trigger':'focus', 'title': $title});
	// });

	// Nav tab on key press change
	$(document).keydown(function(e) {		
		if(e.altKey){
			$("a[data-key="+e.keyCode+"]").click();			
		}
	});


});