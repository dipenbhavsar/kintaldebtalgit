﻿// global var
var liConditionId = 0;
var liIdPrefix = "conditionId_";

// BindOpertaorDropDown(): to Bind Operator Dropdown for Filter Control
function BindOpertaorDropDown() {
    var data = [
                  { text: "Equal", value: "Equal" },
                  { text: "Does not equal", value: "Does not equal" },
                  { text: "Contains", value: "Contains" },
                  { text: "Is less than", value: "Is less than" },
                  { text: "Is less than or equal to", value: "Is less than or equal to" },
                  { text: "Is greater than", value: "Is greater than" },
                  { text: "Is greater than or equal to", value: "Is greater than or equal to" },
                  { text: "Is blank", value: "Is blank" },
                  { text: "Is not blank", value: "Is not blank" },
                  { text: "Between", value: "Between" },
                  { text: "Not between", value: "Not between" },
                  { text: "In", value: "In" },
                  { text: "Not in", value: "Not in" }
    ];

    // create DropDownList from input HTML element
    var ddl = $("#ddlOpertators").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        index: 0,
        optionLabel: "-- Choose Operator --",
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        height: 500,
        select: function (e) {
            if (e.item.text().toString().toUpperCase() == "-- CHOOSE OPERATOR --") {
                $('.filterOperatorsPopup').hide();
                $("#transparentDiv").hide();
            }
            else {
                // To prevent selection of invalid operator for selected datatype
                var crRow = $("#hdnFilterOpertaorIndex").val();
                var crLi = $('#' + crRow);
                var txt = e.item.text().toString().toUpperCase();
                if ($(crLi).attr('data-dbtype').toString().toUpperCase() == "SYSTEM.BOOLEAN") {
                    if (txt == "IS LESS THAN" ||
                        txt == "IS LESS THAN OR EQUAL TO" ||
                        txt == "IS GREATER THAN" ||
                        txt == "IS GREATER THAN OR EQUAL TO" ||
                        txt == "BETWEEN" ||
                        txt == "NOT BETWEEN" ||
                        txt == "IN" ||
                        txt == "NOT IN") {
                        //HideAllPopup();
                        $('.filterOperatorsPopup').hide();
                        $("#transparentDiv").hide();
                        e.preventDefault();
                    }
                }
            }
        },
        change: function (e) {
            var val = this.value();
            SetFilterOpertaorValue(val);
        }
    }).bind("close", function (e) {
        e.preventDefault();
    });
    $("#ddlOpertators_listbox").closest('div').parent('div').css({ 'border': '0px', 'box-shadow': '', '-webkit-box-shadow': '' });
}

// BindLeftMenuDropDown(): to Bind ddlLeftMenu Dropdown for Filter Control
function BindLeftMenuDropDown() {
    var dataLeftMenu = [
                  { text: "Add Condition", value: "Add Condition", grpb: "gr1" },
                  { text: "Add Group", value: "Add Group", grpb: "gr1" },
                  { text: "Remove Row", value: "Remove Row", grpb: "gr2" }
    ];

    // create DropDownList from input HTML element
    var ddl = $("#ddlLeftMenu").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: dataLeftMenu,
        index: 0,
        optionLabel: "-- Choose Action --",
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        group: { field: "grpb" },
        height: 500,
        select: function (e) {
            if (e.item.text().toString().toUpperCase() == "-- CHOOSE ACTION --") {
                $('.filterLeftMenuPopup').hide();
                $("#transparentDiv").hide();
            }
        },
        change: function (e) {
            var val = this.value();
            LeftMenuChanged_ForFilter(val);
        }
    }).bind("close", function (e) {
        e.preventDefault();
    });
    $("#ddlLeftMenu_listbox").closest('div').parent('div').css({ 'border': '0px', 'box-shadow': '', '-webkit-box-shadow': '' });
    $("#ddlLeftMenu_listbox li:last").css({ 'border': '0px', 'border-top': '2px solid #e6e6e6' });
}

// BindAndOrOperatorDropDown(): to Bind ddlAndOrOperator Dropdown for Filter Control
function BindAndOrOperatorDropDown() {
    var dataAddOrOptions = [
                  { text: "AND", value: "AND" },
                  { text: "OR", value: "OR" },
                  { text: "NOT AND", value: "NOT AND" },
                  { text: "NOT OR", value: "NOT OR" }
    ];

    // create DropDownList from input HTML element
    var ddl = $("#ddlAndOrOperator").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: dataAddOrOptions,
        index: 0,
        optionLabel: "-- Choose Operator --",
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        height: 500,
        select: function (e) {
            if (e.item.text().toString().toUpperCase() == "-- CHOOSE OPERATOR --") {
                $('.filterAndOrOperatorPopup').hide();
                $("#transparentDiv").hide();
            }
        },
        change: function (e) {
            var val = this.value();
            AndOrOperatorChanged_ForFilter(val);
        }
    }).bind("close", function (e) {
        e.preventDefault();
    });
    $("#ddlAndOrOperator_listbox").closest('div').parent('div').css({ 'border': '0px', 'box-shadow': '', '-webkit-box-shadow': '' });
}

//AndOrOperatorChanged_ForFilter(selectedAndOrOperator): This function will invoke when user changed And Or operator from Popup
function AndOrOperatorChanged_ForFilter(selectedAndOrOperator) {
    try {
        // selectedAndOrOperator : selected And Or opertaor from popup from filter control

        // selectedMenuRow: give row index of left menu
        var selectedOprtrRow = $("#hdnFilterAndOrOperatorIndex").val();
        var current_LI = $('#' + selectedOprtrRow);
        $(current_LI).find('.AndOrOperator:eq(0)').html(selectedAndOrOperator);

        $("#ddlAndOrOperator").val("").data("kendoDropDownList").text("");
        $("#ddlAndOrOperator").data("kendoDropDownList").select(0)
        HideAllPopup();


    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

//LeftMenuChanged_ForFilter(selectedMenu): This function will invoke when user changed menu from Left Menu Popup
function LeftMenuChanged_ForFilter(selectedMenu) {
    try {
        // selectedMenu : selected menu from left menu popup from filter control
        if ($("#ddlFilterColumns").data("kendoDropDownList").dataSource.data().length > 0) {
            // selectedMenuRow: give row index of left menu
            var selectedMenuRow = $("#hdnFilterLeftMenuIndex").val();

            if (selectedMenu.toString().toUpperCase() == "REMOVE ROW") {
                $('#' + selectedMenuRow).remove();
            }
            else if (selectedMenu.toString().toUpperCase() == "ADD CONDITION") {
                var li = GetHTMLforFilterCriteria();
                //$('#ulFilterConditionList li.liFilterCondition:eq(' + selectedMenuRow + ')').after(li);

                if ($('#' + selectedMenuRow).find('ul').length > 0) {
                    $('#' + selectedMenuRow).find('ul:eq(0)').append(li);
                }
                else {
                    $('#' + selectedMenuRow).after(li);
                }

                $('.cust-ul-tree').find('li:has(ul)').addClass('parent_li');
                KendoTreeBind();
            }
            else if (selectedMenu.toString().toUpperCase() == "ADD GROUP") {

                var liAddGroup = GetHTMLforAddGroup();
                var lstId = liConditionId - 1;
                var liId = liIdPrefix + lstId.toString();

                if ($('#' + selectedMenuRow).find('ul').length > 0) {
                    $('#' + selectedMenuRow).find('ul:eq(0)').append(liAddGroup);
                }
                else {
                    $('#' + selectedMenuRow).after(liAddGroup);
                }

                $('.cust-ul-tree').find('li:has(ul)').addClass('parent_li');
                KendoTreeBind();

                // START -NEW CONDITION IS AUTO ADDED BENEATH THE THAT GROUP WITH FIRST COLUNM NAME FROM VIEW

                var li = GetHTMLforFilterCriteria();
                var lstIdInner = liConditionId - 1;
                var liIdInner = liIdPrefix + lstIdInner.toString();
                if ($('#' + liId).find('ul').length > 0) {
                    $('#' + liId).find('ul:eq(0)').append(li);
                }
                else {
                    $('#' + liId).after(li);
                }

                var current_LI = $('#' + liIdInner);
                var firstColunmName = $("#ddlFilterColumns").data("kendoDropDownList").dataSource.data()[0].value;
                var firstColunmDataType = $("#ddlFilterColumns").data("kendoDropDownList").dataSource.data()[0].sqlDataType;
                $(current_LI).find('.fltrColNameValue').html(firstColunmName);
                $(current_LI).attr('data-dbtype', firstColunmDataType);

                $('.cust-ul-tree').find('li:has(ul)').addClass('parent_li');
                KendoTreeBind();

                // END -NEW CONDITION IS AUTO ADDED BENEATH THE THAT GROUP WITH FIRST COLUNM NAME FROM VIEW
            }
            $("#ddlLeftMenu").val("").data("kendoDropDownList").text("");
            $("#ddlLeftMenu").data("kendoDropDownList").select(0);
        }
        else {
            jAlert("No Columns Found For Selected List Type !", 'Information Dialog');
        }
        HideAllPopup();


    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

//SetFilterOpertaorValue(selectedOprtr): This function will invoke when user select operator from filter popup - this will close operator popup and set opertaor value to the current row
function SetFilterOpertaorValue(selectedOprtr) {
    try {
        // selectedOprtr : selected opertaor value from popup dropdown
        selectedOprtr = selectedOprtr.trim() == "" ? "&lt;Select Operator&gt;" : selectedOprtr;
        var selectedOpRow = $("#hdnFilterOpertaorIndex").val();
        var current_LI = $('#' + selectedOpRow);
        $(current_LI).find('.fltrOperator').html(selectedOprtr);
        HideAllPopup();
        SetInputTypeBasedOnOperator(selectedOpRow, selectedOprtr, current_LI);
        $("#ddlOpertators").val("").data("kendoDropDownList").text("");
        $("#ddlOpertators").data("kendoDropDownList").select(0)

    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

//SetFilterColumnsValue(selectedColumn, ColDataType): This function will invoke when user select Column Name from filter popup - this will close Column Name popup and set Column Name value to the current row
function SetFilterColumnsValue(selectedColumn, ColDataType) {
    try {
        // selectedColumn : selected Column name from popup dropdown        
        // ColDataType: selected Column's DataType

        selectedColumn = selectedColumn.trim() == "" ? "&lt;Select Column&gt;" : selectedColumn;
        var selectedOpRow = $("#hdnFilterColumnsIndex").val();
        var current_LI = $('#' + selectedOpRow);
        $(current_LI).find('.fltrColNameValue').html(selectedColumn);
        $(current_LI).attr('data-dbtype', ColDataType);
        if ($(current_LI).find('.filterValueText').length > 0) {
            $(current_LI).find('.filterValueText').val("");
        }
        HideAllPopup();
        $("#ddlFilterColumns").val("").data("kendoDropDownList").text("");
        $("#ddlFilterColumns").data("kendoDropDownList").select(0)
    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

// BindFilterColumnsDropDown(): to Bind Columns Name Dropdown for Filter Control
function BindFilterColumnsDropDown(contactListTypeId) {
    // contactListTypeId : selected Contact List Type Id from previous tab

    $.ajax({
        type: "POST",
        //url: encodeURI('@Url.Action("GetFilterColumnsNameList", "CRM_N")'),
        url: "/CRM_N/GetFilterColumnsNameList",
        cache: false,
        dataType: "json",
        data: { "contactListTypeId": contactListTypeId },
        error: function (jqXHR, exception) {
            jAlert(exception.toString(), 'Information Dialog');
        },
        success: function (data) {
            data = JSON.parse(data);
            if (data[0] == true) {
                var colList = data[1];
                var listArray = [];
                $.each(colList, function (key, value) {
                    //alert(key + ": " + value);
                    var sObj = {};
                    sObj.text = key;
                    sObj.value = key;
                    sObj.sqlDataType = value;
                    listArray.push(sObj);
                });
                // create DropDownList from input HTML element
                var ddl = $("#ddlFilterColumns").kendoDropDownList({
                    dataTextField: "text",
                    dataValueField: "value",
                    dataSource: listArray,
                    index: 0,
                    optionLabel: "-- Choose Column --",
                    animation: {
                        close: {
                            effects: "fadeOut zoom:out",
                            duration: 300
                        },
                        open: {
                            effects: "fadeIn zoom:in",
                            duration: 300
                        }
                    },
                    height: 328,
                    select: function (e) {
                        if (e.item.text().toString().toUpperCase() == "-- CHOOSE COLUMN --") {
                            $('.filterColumnsPopup').hide();
                            $("#transparentDiv").hide();
                        }
                    },
                    change: function (e) {
                        var val = this.value();
                        var ColDataType = this.dataItem().sqlDataType
                        SetFilterColumnsValue(val, ColDataType);
                    }
                }).bind("close", function (e) {
                    e.preventDefault();
                });
                $("#ddlFilterColumns_listbox").closest('div').parent('div').css({ 'border': '0px', 'box-shadow': '', '-webkit-box-shadow': '' });
            }
            else {
                jAlert('Error Reason : ' + data[1].toString(), 'Information Dialog');
            }
        }
    });
}

// CheckListType(): This function will invoke when user click on List Building Criteria Tab - to load Colunm List Popup for Filter
function CheckListType() {
    try {
        //if (!$("#LBC").hasClass('active')) {
        //    $("#ul_filter_tree").empty();
        //}
        if ($("#ContactListTypeId").val().trim() == "") {
            jAlert('Please Select Contact List Type First', 'Information Dialog', function () {
                $("#ancDetails").tab('show');
            });
        }
        else {
            if (!$("#LBC").hasClass('active')) {
                LoadListTypeFilterData();
            }
        }
    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

// LoadListTypeFilterData(): load Colunm List Popup for Filter
function LoadListTypeFilterData() {
    try {
        $("#lbcListTypeDescription").html($("#ContactListTypeDescription").val());
        //$("#contactGrid").hide();
        BindFilterColumnsDropDown($("#ContactListTypeId").val());
    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

//AddNewFilter(): this function will add new filter condition in Filter Control
function AddNewFilter() {
    try {
        if ($("#ddlFilterColumns").data("kendoDropDownList").dataSource.data().length > 0) {
            var li = GetHTMLforFilterCriteria();

            if ($("#ul_filter_tree li").length == 0) {
                $("#ul_filter_tree").append(li);
            }
            else {
                var bottomliId = GetBottomMostLiFromTree();
                $('#' + bottomliId).after(li);
            }


            $('.cust-ul-tree').find('li:has(ul)').addClass('parent_li');
            KendoTreeBind();
            if ($("#ul_filter_tree li").length > 0) {
                $('#divFilterConditions').animate({
                    scrollTop: $("#ul_filter_tree li:last").offset().top
                }, 100);
            }
        }
        else {
            jAlert("No Columns Found For Selected List Type !", 'Information Dialog');
        }
    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

//GetHTMLforAddGroup(): this function will returns HTML for Add Group functionality
function GetHTMLforAddGroup() {
    var liId = GetGeneratedLiId();
    var retObj = "<li id=" + liId + " class='liFilterCondition addGroup' ><div class='divCondition'  >" +
                    "<div class='filterNode'  onclick='showLeftMenu(event,this)' > ...</div>" +
                    "<span> " +
                        "<span class='AndOrOperator'  onclick='showAndOrOperator(event,this)'> AND" +
                        "</span>" +
                        "<span> &nbsp;applies to the following conditions" +
                        "</span>" +
                    "</span></div>" +
                    "<ul class='groupul'></ul>"
    "</li>";


    return retObj;
}

//GetHTMLforFilterCriteria(): this function will returns HTML for LI in Filter Control
function GetHTMLforFilterCriteria() {
    var liId = GetGeneratedLiId();
    var retObj = "<li id=" + liId + " class='liFilterCondition' data-dbtype=''><div class='divCondition' >" +
                    "<div class='fltrColunm' >" +
                        "<div class='filterNode' onclick='showLeftMenu(event,this)' > ...</div>" +
                        "&nbsp;&nbsp;<span class='fltrColNameValue' onclick='showColumnsList(event,this)'>&lt;Select Column&gt;</span>" +
                    "</div>" +
                    "<div class='fltrValue' >" +
                        "<input type='text' class='filterValueText' Placeholder='<Empty>' defaultValue='demo'>" +
                    "</div>" +
                    "<div class='fltrOperator' onclick='showOperator(event,this)' >" +
                        "&lt;Select Operator&gt;" +
                    "</div>" +
                    "</div>" +
                "</li>";
    //var retObj = "<li id=" + liId + " class='liFilterCondition'  data-dbtype=''>" +
    //                "<table>" +
    //                    "<tr>" +
    //                        "<td>" +
    //                            "<div style='display:inline;'  onclick='showLeftMenu(event,this)' > ...</div>" +  
    //                        "</td>" +
    //                        "<td>" +
    //                            "&nbsp;&nbsp;<span  onclick='showColumnsList(event,this)'>&lt;Select Column&gt;</span>" +
    //                        "</td>" +
    //                        "<td onclick='showOperator(event,this)'>" +
    //                            "&lt;Select Operator&gt;" +
    //                        "</td>" +
    //                        "<td>" +
    //                            "<input type='text'  Placeholder='<Empty>'>" +
    //                        "</td>" +
    //                    "</tr>" +
    //                "</table>" +
    //            "</li>"; // WORKING

    return retObj;
}

//GetGeneratedLiId(): This will generate unique id for each <li>
function GetGeneratedLiId() {
    var liId = liIdPrefix + liConditionId.toString();
    liConditionId++;
    return liId;
}

//SetInputTypeBasedOnOperator(): this function will set the Input type control based on operator
function SetInputTypeBasedOnOperator(rwIndx, selectedOperator, current_LI) {
    try {
        // rwIndx: Index of selected filter row
        // selectedOperator: Selected Operator from popup
        // current_LI: current <li> html object
        $(current_LI).find('.fltrValue').html("");
        var inputTypeHTML = "";
        switch (selectedOperator.toString().toUpperCase()) {
            case "EQUAL":
            case "DOES NOT EQUAL":
            case "CONTAINS":
            case "IS LESS THAN":
            case "IS LESS THAN OR EQUAL TO":
            case "IS GREATER THAN":
            case "IS GREATER THAN OR EQUAL TO":
                {
                    inputTypeHTML = "<input type='text' class='filterValueText' Placeholder='<Empty>'>";
                    break;
                }
            case "BETWEEN":
            case "NOT BETWEEN":
                {
                    inputTypeHTML = "<span>( <input type='text' class='filterValueText smallTbox rangeone' Placeholder='<Empty>'> &nbsp;<span class='btwnAnd'>and</span>&nbsp; <input type='text' class='filterValueText smallTbox rangetwo' Placeholder='<Empty>'> )</span>";
                    break;
                }
            case "IS BLANK":
            case "IS NOT BLANK":
                {
                    inputTypeHTML = "";
                    break;
                }
            case "IN":
            case "NOT IN":
                {
                    inputTypeHTML = "( <span class='outerSpan'> <span class='singleRowSpan'> <input type='text' value='' class='filterValueText smallTbox' Placeholder='<Empty>'> &nbsp;,&nbsp; <input type='text' value='' class='filterValueText smallTbox' Placeholder='<Empty>'> &nbsp; <span class='closingBracket'>)&nbsp;</span></span><span title='Add' onclick='AddInputText(this)' class='InAdd'>+</span></span>";
                    break;
                }
        }

        $(current_LI).find('.fltrValue').html(inputTypeHTML);

    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

//AddInputText(): This will add multiple text box for IN and NOT IN operators
function AddInputText(curSpan) {
    try {
        $(curSpan).closest('.outerSpan').find('.singleRowSpan').find('span.closingBracket').remove();
        var singleRowSpanHtml = ",&nbsp;<input type='text' class='filterValueText smallTbox' Placeholder='<Empty>'> &nbsp;<span class='closingBracket'>)</span>&nbsp;";
        $(curSpan).closest('.outerSpan').find('.singleRowSpan').append(singleRowSpanHtml);
    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

// showLeftMenu(): This function will invoke when user click on "..." button from Filter Control
function showLeftMenu(event, curobj) {
    try {
        var cnt = $(curobj).closest("li.liFilterCondition").attr('id');
        $("#hdnFilterLeftMenuIndex").val(cnt);
        $("#transparentDiv").show();
        $('.filterLeftMenuPopup').css('left', event.pageX);
        $('.filterLeftMenuPopup').css('top', event.pageY);
        $('.filterLeftMenuPopup').fadeIn('slow');
        $(".filterLeftMenuPopup").css("position", "absolute");
        $("#ddlLeftMenu").data("kendoDropDownList").open();
    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

// showOperator(): This function will invoke when user click on <Select Operator > link from Filter Control
function showOperator(event, curobj) {
    try {
        var cnt = $(curobj).closest("li.liFilterCondition").attr('id');
        $("#hdnFilterOpertaorIndex").val(cnt);
        $("#transparentDiv").show();
        $('.filterOperatorsPopup').css('left', event.pageX);
        $('.filterOperatorsPopup').css('top', event.pageY);
        $('.filterOperatorsPopup').fadeIn('slow');
        $(".filterOperatorsPopup").css("position", "absolute");
        $("#ddlOpertators").data("kendoDropDownList").open();
    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

// showColumnsList(): This function will invoke when user click on <Select Column > link from Filter Control
function showColumnsList(event, curobj) {
    try {
        var cnt = $(curobj).closest("li.liFilterCondition").attr('id');
        $("#hdnFilterColumnsIndex").val(cnt);
        $("#transparentDiv").show();
        $('.filterColumnsPopup').css('left', event.pageX);
        $('.filterColumnsPopup').css('top', event.pageY);
        $('.filterColumnsPopup').fadeIn('slow');
        $(".filterColumnsPopup").css("position", "absolute");
        $("#ddlFilterColumns").data("kendoDropDownList").open();
    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

// showAndOrOperator(): This function will invoke when user click on "AND" link from Filter Control
function showAndOrOperator(event, curobj) {
    try {
        var cnt = $(curobj).closest("li.liFilterCondition").attr('id');
        $("#hdnFilterAndOrOperatorIndex").val(cnt);
        $("#transparentDiv").show();
        $('.filterAndOrOperatorPopup').css('left', event.pageX);
        $('.filterAndOrOperatorPopup').css('top', event.pageY);
        $('.filterAndOrOperatorPopup').fadeIn('slow');
        $(".filterAndOrOperatorPopup").css("position", "absolute");
        $("#ddlAndOrOperator").data("kendoDropDownList").open();
    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}


//RecursiveGetGroupValue(): This function is recursively called to find the inner most group from ul li html tags and build query
function RecursiveGetGroupValue(curGrpul) {
    try {
        //curGrpul: this is <ul> object
        var gnQuery = "";
        var liList = $(curGrpul).children('li');
        // if find ul in curGrpul then call RecursiveGetGroupValue()
        $.each(liList, function () {
            if ($(this).hasClass('addGroup')) {
                // For Group Conditions 
                var groupOperator = $(this).find('.AndOrOperator:eq(0)').text();
                if (groupOperator == "NOT AND") {
                    groupOperator = "AND NOT";
                }
                if (groupOperator == "NOT OR") {
                    groupOperator = "OR NOT";
                }

                var innerQuery = RecursiveGetGroupValue($(this).find('.groupul'));
                if (innerQuery.trim() != "") {
                    gnQuery += " " + groupOperator + " ( ";
                    gnQuery += " " + innerQuery + " ) ";
                }
            }
            else {
                // For Normal Conditions (without group)                
                if (!$(this).hasClass("queryGenerated")) {
                    $(this).addClass("queryGenerated");
                    var opAndValue = GetOperatorAndFilteredValue($(this));
                    if (gnQuery != "") {
                        gnQuery += " AND  ";
                    }
                    gnQuery += " (" + $(this).find('.fltrColNameValue').text() + " " + opAndValue + ") ";
                }
            }
        });

        return gnQuery;

    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

//GenerateQuery(): This function will Generate Query based on filter
function GenerateQuery() {
    try {
        //if (ValidateFilters()) {
        $(".queryGenerated").removeClass("queryGenerated");
        var liList = $("#ul_filter_tree").children('li'); // root level

        var gnQuery = "WHERE 1 = 1";
        $.each(liList, function () {
            if ($(this).hasClass('addGroup')) {
                // For Group Conditions 
                var groupOperator = $(this).find('.AndOrOperator:eq(0)').text();
                if (groupOperator == "NOT AND") {
                    groupOperator = "AND NOT";
                }
                if (groupOperator == "NOT OR") {
                    groupOperator = "OR NOT";
                }

                var innerQuery = RecursiveGetGroupValue($(this).find('.groupul'));
                if (innerQuery.trim() != "") {
                    gnQuery += " " + groupOperator + " ( ";
                    gnQuery += " " + innerQuery + " ) ";
                }
            }
            else {
                // For Normal Conditions (without group)         
                if (!$(this).hasClass("queryGenerated")) {
                    $(this).addClass("queryGenerated");
                    var opAndValue = GetOperatorAndFilteredValue($(this));
                    gnQuery += " AND (" + $(this).find('.fltrColNameValue').text() + " " + opAndValue + ") ";
                }
            }
        });

        //jAlert(gnQuery, 'Information Dialog');
        return gnQuery;
        // }
    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

// GetOperatorAndFilteredValue(): this will returns Operator and FilteredValue from the textbox
function GetOperatorAndFilteredValue(singleCriteria) {
    try {
        // singleCriteria : single HTML li tag object

        var oprtr = $(singleCriteria).find('.fltrOperator').text();
        var retObj = "";
        switch (oprtr.toString().toUpperCase()) {
            case "EQUAL":
                {
                    var txtVal = $(singleCriteria).find('.filterValueText').val().trim().replace(/'/g, "''");
                    if ($(singleCriteria).attr('data-dbtype').toString().toUpperCase() == "SYSTEM.BOOLEAN") {
                        txtVal = txtVal.toUpperCase() == "Y" ? "1" : "0";
                    }
                    retObj = " = ";
                    retObj += "'" + txtVal + "'";
                    break;
                }
            case "DOES NOT EQUAL":
                {
                    var txtVal = $(singleCriteria).find('.filterValueText').val().trim().replace(/'/g, "''");
                    if ($(singleCriteria).attr('data-dbtype').toString().toUpperCase() == "SYSTEM.BOOLEAN") {
                        txtVal = txtVal.toUpperCase() == "Y" ? "1" : "0";
                    }
                    retObj = " != ";
                    retObj += "'" + txtVal + "'";
                    break;
                }
            case "CONTAINS":
                {
                    retObj = " LIKE ";
                    retObj += "'%" + $(singleCriteria).find('.filterValueText').val().trim().replace(/'/g, "''") + "%'";
                    break;
                }
            case "IS LESS THAN":
                {
                    retObj = " < ";
                    retObj += "'" + $(singleCriteria).find('.filterValueText').val().trim().replace(/'/g, "''") + "'";
                    break;
                }
            case "IS LESS THAN OR EQUAL TO":
                {
                    retObj = " <= ";
                    retObj += "'" + $(singleCriteria).find('.filterValueText').val().trim().replace(/'/g, "''") + "'";
                    break;
                }
            case "IS GREATER THAN":
                {
                    retObj = " > ";
                    retObj += "'" + $(singleCriteria).find('.filterValueText').val().trim().replace(/'/g, "''") + "'";
                    break;
                }
            case "IS GREATER THAN OR EQUAL TO":
                {
                    retObj = " >= ";
                    retObj += "'" + $(singleCriteria).find('.filterValueText').val().trim().replace(/'/g, "''") + "'";
                    break;
                }
            case "BETWEEN":
                {
                    retObj = " BETWEEN ";
                    retObj += "'" + $(singleCriteria).find('.rangeone').val().trim().replace(/'/g, "''") + "' AND '" + $(singleCriteria).find('.rangetwo').val().trim().replace(/'/g, "''") + "'";
                    break;
                }
            case "NOT BETWEEN":
                {
                    retObj = " NOT BETWEEN ";
                    retObj += "'" + $(singleCriteria).find('.rangeone').val().trim().replace(/'/g, "''") + "' AND '" + $(singleCriteria).find('.rangetwo').val().trim().replace(/'/g, "''") + "'";
                    break;
                }
            case "IS BLANK":
                {
                    retObj = " IS NULL ";
                    break;
                }
            case "IS NOT BLANK":
                {
                    retObj = " IS NOT NULL ";
                    break;
                }
            case "IN":
                {
                    retObj = " IN ";

                    var alltxts = $(singleCriteria).find('.filterValueText');
                    var alltxtsValues = $(alltxts).map(function () {
                        return "'" + $(this).val().trim().replace(/'/g, "''") + "'";
                    }).get().join(',');

                    retObj += "(" + alltxtsValues + ")";
                    break;
                }
            case "NOT IN":
                {
                    retObj = " NOT IN ";

                    var alltxts = $(singleCriteria).find('.filterValueText');
                    var alltxtsValues = $(alltxts).map(function () {
                        return "'" + $(this).val().trim().replace(/'/g, "''") + "'";
                    }).get().join(',');

                    retObj += "(" + alltxtsValues + ")";
                    break;
                }
        }
        return retObj;
    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

//ValidateFilters(): this function will validate all rows of filter controls
function ValidateFilters() {
    try {
        var isChk = true;
        var errorMsg = "";
        var liListValidate = $("#ul_filter_tree").find('li.liFilterCondition');
        var errorObject = null;

        if ($("#ddlFilterColumns").data("kendoDropDownList").dataSource.data().length <= 0) {
            errorMsg = "No Columns Found For Selected List Type !";
            isChk = false;
        }

        $.each(liListValidate, function () {
            var row = $(this).index() + 1;
            if (!$(this).hasClass('addGroup')) {
                if ($(this).find('.fltrColNameValue').text().trim() == "<Select Column>" || $(this).find('.fltrColNameValue').text().trim() == "") {
                    errorObject = $(this);
                    errorMsg = "Please Select Column Name.";
                    isChk = false;
                    return false; // break; each loop here
                }
                if ($(this).find('.fltrOperator').text().trim() == "<Select Operator>" || $(this).find('.fltrOperator').text().trim() == "") {
                    errorMsg = "Please Select Operator.";
                    errorObject = $(this);
                    isChk = false;
                    return false; // break; each loop here
                }
                if ($(this).find('.fltrOperator').text().trim().toUpperCase() == "BETWEEN" || $(this).find('.fltrOperator').text().trim().toUpperCase() == "NOT BETWEEN") {
                    if ($(this).find('.rangeone').val().trim() == "" || $(this).find('.rangetwo').val().trim() == "") {
                        errorMsg = "Please Enter Value Of Filter.";
                        errorObject = $(this);
                        isChk = false;
                        return false; // break; each loop here
                    }
                }
                if ($(this).find('.fltrOperator').text().trim().toUpperCase() == "IS LESS THAN" ||
                    $(this).find('.fltrOperator').text().trim().toUpperCase() == "IS LESS THAN OR EQUAL TO" ||
                    $(this).find('.fltrOperator').text().trim().toUpperCase() == "IS GREATER THAN" ||
                    $(this).find('.fltrOperator').text().trim().toUpperCase() == "IS GREATER THAN OR EQUAL TO") {
                    if ($(this).find('.filterValueText').val().trim() == "") {
                        errorMsg = "Please Enter Value Of Filter.";
                        errorObject = $(this);
                        isChk = false;
                        return false; // break; each loop here
                    }
                }
            }
        });

        if (isChk) { // If no error found from above then it will check for Group conditions
            // to check Group can not be empty.
            var allInnerUls = $("#ul_filter_tree").find('ul.groupul');
            $.each(allInnerUls, function () {
                if ($(this).find('li').length == 0) {
                    errorMsg = "Group can not be empty.";
                    errorObject = $(this).closest('li');
                    isChk = false;
                    return false; // break; each loop here
                }
            });
        }

        if ($("#ContactListTypeId").val().trim() == "") {
            errorMsg = "'Please Select Contact List Type First.";
            isChk = false;
        }

        if (!isChk) {
            jAlert(errorMsg, 'Information Dialog', function () {
                if (errorObject != null) {
                    $(errorObject).addClass("error_li");
                    setTimeout(function () {
                        $(errorObject).removeClass("error_li");
                    }, 2000);
                }
                return false;
            });
        }
        else {
            return true;
        }

    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

// HideAllPopup(): This will hide all filter popup (ColumnsList and Operator List )
function HideAllPopup() {
    try {
        $('.filterOperatorsPopup').hide();
        $('.filterColumnsPopup').hide();
        $('.filterLeftMenuPopup').hide();
        $('.filterAndOrOperatorPopup').hide();
        $("#transparentDiv").hide();
        $("#hdnFilterLeftMenuIndex").val('');
        $("#hdnFilterOpertaorIndex").val('');
        $("#hdnFilterColumnsIndex").val('');
        $("#hdnFilterAndOrOperatorIndex").val('');
    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

//ClearFilter(): This will clear all filter condition and make grid empty
function ClearFilter() {
    $("#ul_filter_tree").empty();
    liConditionId = 0;
    HideAllPopup();

    if ($("#ContactListTypeId").val() == "1") {
        $("#gridLBCContact").data("kendoGrid").dataSource.read({ 'gQuery': '' });
        $("#contactGrid").hide();
    }
}

function ApplyFilter() {
    try {
        if (ValidateFilters()) {
            //
            //alert($("#ul_filter_tree").html());
            //return;
            //
            var generatedQury = GenerateQuery();
            var filterObj = {};
            filterObj.generatedQuery = generatedQury;
            filterObj.listTypeId = $("#ContactListTypeId").val();

            if ($("#ContactListTypeId").val() == "1") {
                $("#gridLBCContact").data("kendoGrid").dataSource.read({ 'gQuery': generatedQury });
                $("#contactGrid").show();
            }
        }
    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

// GetBottomMostLiFromTree(): this will get id of bottom most li from entire tree.
function GetBottomMostLiFromTree() {
    try {
        var bottomMostLi;
        //var allInnerUl = $("#ul_filter_tree").find('ul.groupul');
        //var lastLiArray = [];
        //if (allInnerUl.length > 0) {
        //    $.each(allInnerUl, function () {
        //        if ($(this).find('li.liFilterCondition').length > 0) {
        //            lastLiArray.push($(this).find('li:last').attr('id'));
        //        }
        //    });
        //    if (lastLiArray.length > 0) {
        //        bottomMostLi = lastLiArray[0];
        //    }
        //    else {
        //        bottomMostLi = $("#ul_filter_tree li:last").attr('id');
        //    }
        //}
        //else {
        //    bottomMostLi = $("#ul_filter_tree li:last").attr('id');
        //}
        bottomMostLi = $("#ul_filter_tree li.liFilterCondition:last").attr('id');
        return bottomMostLi;
    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

// OnKeyPressValueInputs(): This function will prevent user to enter invalid data in input textbox
function OnKeyPressValueInputs(e, curInputTxt) {
    try {
        // e : current event
        // curInputTxt: Current text box object

        var dbtype = $(curInputTxt).closest('li.liFilterCondition').attr('data-dbtype');
        var key = e.charCode || e.keyCode || 0;
        //alert(key)
        if (dbtype.toString().toUpperCase() == "SYSTEM.DECIMAL") {
            if (
                key == 8 ||
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105)) {

                //$(curInputTxt).attr('value',$(curInputTxt).val());
            }
            else {
                e.preventDefault();
            }
        }
        else if (dbtype.toString().toUpperCase() == "SYSTEM.INT32") {
            if (
                key == 8 ||
                key == 9 ||
                key == 13 ||
                key == 46 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105)) {

                //$(curInputTxt).attr('value', $(curInputTxt).val());
            }
            else {
                e.preventDefault();
            }
        }
        else if (dbtype.toString().toUpperCase() == "SYSTEM.BOOLEAN") {
            if (
                key == 8 || key == 78 || key == 89 || key == 39 || key == 36 ||
                key == 37 || key == 16 || key == 17 || key == 46) {

                if ($(curInputTxt).val().toString().length > 0) {
                    if (key == 78 || key == 89) {
                        e.preventDefault();
                    }
                    //$(curInputTxt).attr('value', $(curInputTxt).val());
                }
                //$(curInputTxt).attr('value', $(curInputTxt).val());
            }
            else {
                e.preventDefault();
            }
        }
        else if (dbtype.toString().toUpperCase() == "SYSTEM.STRING") {
            // ALLOW USER TO TYPE 
            //$(curInputTxt).attr('value', $(curInputTxt).val());
        }
        else {
            e.preventDefault();
        }

    } catch (err) {
        jAlert(err, 'Information Dialog');
    }
}

function KendoTreeBind() {
    $("#ul_filter_tree").kendoTreeView();
    $(".cust-ul-tree").find('.k-treeview').css({ 'height': '285px' });
}