--==========================================================================================================================================
--==========================================================================================================================================


ALTER TABLE Lab ADD Description	nvarchar(1024)
ALTER TABLE Lab ADD IsDeleted	bit
ALTER TABLE Lab ADD CreatedById	int
ALTER TABLE Lab ADD EntryDate	datetime
ALTER TABLE Lab ADD ModifiedById	int
ALTER TABLE Lab ADD ModifiedDate	datetime

--==========================================================================================================================================
--==========================================================================================================================================


DROP PROCEDURE [dbo].[wp_sp_InsertUpdateDrugMaster]
GO


CREATE PROCEDURE [dbo].[wp_sp_InsertUpdateDrugMaster]
@Id bigint
,@DrugName nvarchar(512)
,@GenericName nvarchar(512) = NULL
,@Description nvarchar(1024) = NULL
,@CreatedById int = null
,@ModifiedById int = null
,@ReturnValue INT OUTPUT
AS
BEGIN
	BEGIN TRY	
	IF(@Id > 0)
		BEGIN	
		
			IF ((SELECT COUNT(*)
                 FROM [DrugMaster]
                 Where [DrugName] = @DrugName and Id != @Id and IsDeleted = 0) > 0 )
            BEGIN
                RAISERROR('%s', 18, 1, 'Drug name already exists.')
            END

            UPDATE [dbo].[DrugMaster]
		   SET 
			  [DrugName] = @DrugName
			  ,[GenericName] = @GenericName
			  ,[Description] = @Description
			  ,[ModifiedById] = @ModifiedById
			  ,[ModifiedDate] = GETDATE()
            WHERE 
            [Id] = @Id
            SELECT @ReturnValue = @Id
    END
    ELSE 
	BEGIN
			IF ((SELECT COUNT(*)
                 FROM [DrugMaster]
                 Where [DrugName] = @DrugName and [IsDeleted] = 0) > 0)

            BEGIN
                RAISERROR('%s', 18, 1, 'Drug name already exists.')
            END

            INSERT [dbo].[DrugMaster]
           (
           [DrugName]
           ,[GenericName]
           ,[Description]
           ,[IsDeleted]
           ,[CreatedById]
           ,[EntryDate]
		   )
            VALUES
            (
           @DrugName
           ,@GenericName
           ,@Description
           ,0
           ,@CreatedById
           ,GETDATE()
            )
            SELECT @ReturnValue = SCOPE_IDENTITY()
	END
	
	END TRY
BEGIN CATCH		
	

	--RAISERROR with severity 11-19 will cause execution to jump to the CATCH block
	DECLARE @Msg NVARCHAR(MAX)  
	SELECT @Msg=ERROR_MESSAGE() 
	RAISERROR('%s', 18, 1,@Msg)
	
END CATCH

END
GO




--==========================================================================================================================================
--==========================================================================================================================================

DROP PROCEDURE [dbo].[wp_sp_getDrugMasterById]
GO

CREATE PROCEDURE [dbo].[wp_sp_getDrugMasterById]
@Id int
AS
BEGIN
	
	SELECT * FROM DrugMaster where Id = @Id and IsDeleted = 0

END
GO

--==========================================================================================================================================
--==========================================================================================================================================

DROP PROCEDURE [dbo].[wp_sp_getDrugMasterList]
GO

CREATE PROCEDURE [dbo].[wp_sp_getDrugMasterList]
AS
BEGIN
	
	SELECT * FROM DrugMaster where  IsDeleted = 0

END
GO

--==========================================================================================================================================
--==========================================================================================================================================

DROP PROCEDURE [dbo].[wp_sp_DeleteDrugMasterById]
GO

CREATE PROCEDURE [dbo].[wp_sp_DeleteDrugMasterById]
@Id INT
AS
BEGIN
	
	UPDATE DrugMaster
	SET IsDeleted = 1
	,[ModifiedDate] = GETDATE() 
	WHERE ID = @Id
END
GO


--==========================================================================================================================================
--==========================================================================================================================================


DROP PROCEDURE [dbo].[wp_sp_InsertUpdateTestMaster]
GO


CREATE PROCEDURE [dbo].[wp_sp_InsertUpdateTestMaster]
@Id bigint
,@TestName nvarchar(512)
,@Description nvarchar(1024) = NULL
,@CreatedById int = null
,@ModifiedById int = null
,@ReturnValue INT OUTPUT
AS
BEGIN
	BEGIN TRY	
	IF(@Id > 0)
		BEGIN	
		
			IF ((SELECT COUNT(*)
                 FROM [TestMaster]
                 Where [TestName] = @TestName and Id != @Id and IsDeleted = 0) > 0 )
            BEGIN
                RAISERROR('%s', 18, 1, 'Test name already exists.')
            END

            UPDATE [dbo].[TestMaster]
		   SET 
			  [TestName] = @TestName
			  ,[Description] = @Description
			  ,[ModifiedById] = @ModifiedById
			  ,[ModifiedDate] = GETDATE()
            WHERE 
            [Id] = @Id
            SELECT @ReturnValue = @Id
    END
    ELSE 
	BEGIN
			IF ((SELECT COUNT(*)
                 FROM [TestMaster]
                 Where [TestName] = @TestName and [IsDeleted] = 0) > 0)

            BEGIN
                RAISERROR('%s', 18, 1, 'Test name already exists.')
            END

            INSERT [dbo].[TestMaster]
           (
           [TestName]
           ,[Description]
           ,[IsDeleted]
           ,[CreatedById]
           ,[EntryDate]
		   )
            VALUES
            (
           @TestName
           ,@Description
           ,0
           ,@CreatedById
           ,GETDATE()
            )
            SELECT @ReturnValue = SCOPE_IDENTITY()
	END
	
	END TRY
BEGIN CATCH		
	

	--RAISERROR with severity 11-19 will cause execution to jump to the CATCH block
	DECLARE @Msg NVARCHAR(MAX)  
	SELECT @Msg=ERROR_MESSAGE() 
	RAISERROR('%s', 18, 1,@Msg)
	
END CATCH

END
GO



--==========================================================================================================================================
--==========================================================================================================================================

DROP PROCEDURE [dbo].[wp_sp_DeleteTestMasterById]
GO

CREATE PROCEDURE [dbo].[wp_sp_DeleteTestMasterById]
@Id INT
AS
BEGIN
	
	UPDATE TestMaster
	SET IsDeleted = 1
	,[ModifiedDate] = GETDATE() 
	WHERE ID = @Id
END
GO


--==========================================================================================================================================
--==========================================================================================================================================

DROP PROCEDURE [dbo].[wp_sp_getTestMasterById]
GO

CREATE PROCEDURE [dbo].[wp_sp_getTestMasterById]
@Id int
AS
BEGIN
	
	SELECT * FROM TestMaster where Id = @Id and IsDeleted = 0

END
GO



--==========================================================================================================================================
--==========================================================================================================================================

DROP PROCEDURE [dbo].[wp_sp_getTestMasterList]
GO

CREATE PROCEDURE [dbo].[wp_sp_getTestMasterList]
AS
BEGIN
	
	SELECT * FROM TestMaster where  IsDeleted = 0

END
GO


--==========================================================================================================================================
--==========================================================================================================================================

DROP PROCEDURE [dbo].[wp_sp_InsertUpdateLabMaster]
GO

CREATE PROCEDURE [dbo].[wp_sp_InsertUpdateLabMaster]
@Id bigint
,@LabName nvarchar(512)
,@Description nvarchar(1024) = NULL
,@CreatedById int = null
,@ModifiedById int = null
,@ReturnValue INT OUTPUT
AS
BEGIN
	BEGIN TRY	
	IF(@Id > 0)
		BEGIN	
		
			IF ((SELECT COUNT(*)
                 FROM [Lab]
                 Where [LabName] = @LabName and Id != @Id and IsDeleted = 0) > 0 )
            BEGIN
                RAISERROR('%s', 18, 1, 'Lab name already exists.')
            END

            UPDATE [dbo].[Lab]
		   SET 
			  [LabName] = @LabName
			  ,[Description] = @Description
			  ,[ModifiedById] = @ModifiedById
			  ,[ModifiedDate] = GETDATE()
            WHERE 
            [Id] = @Id
            SELECT @ReturnValue = @Id
    END
    ELSE 
	BEGIN
			IF ((SELECT COUNT(*)
                 FROM [Lab]
                 Where [LabName] = @LabName and [IsDeleted] = 0) > 0)

            BEGIN
                RAISERROR('%s', 18, 1, 'Lab name already exists.')
            END

            INSERT [dbo].[Lab]
           (
           [LabName]
           ,[Description]
           ,[IsDeleted]
           ,[CreatedById]
           ,[EntryDate]
		   )
            VALUES
            (
           @LabName
           ,@Description
           ,0
           ,@CreatedById
           ,GETDATE()
            )
            SELECT @ReturnValue = SCOPE_IDENTITY()
	END
	
	END TRY
BEGIN CATCH		
	

	--RAISERROR with severity 11-19 will cause execution to jump to the CATCH block
	DECLARE @Msg NVARCHAR(MAX)  
	SELECT @Msg=ERROR_MESSAGE() 
	RAISERROR('%s', 18, 1,@Msg)
	
END CATCH

END
GO



--==========================================================================================================================================
--==========================================================================================================================================


DROP PROCEDURE [dbo].[wp_sp_getLabMasterList]
GO

CREATE PROCEDURE [dbo].[wp_sp_getLabMasterList]
AS
BEGIN
	
	SELECT * FROM Lab where ISNULL(IsDeleted,0) = 0

END
GO


--==========================================================================================================================================
--==========================================================================================================================================

DROP PROCEDURE [dbo].[wp_sp_getLabMasterById]
GO

CREATE PROCEDURE [dbo].[wp_sp_getLabMasterById]
@Id int
AS
BEGIN
	
	SELECT * FROM Lab where Id = @Id and IsDeleted = 0

END
GO



--==========================================================================================================================================
--==========================================================================================================================================

DROP PROCEDURE [dbo].[wp_sp_DeleteLabMasterById]
GO

CREATE PROCEDURE [dbo].[wp_sp_DeleteLabMasterById]
@Id INT
AS
BEGIN
	
	UPDATE Lab
	SET IsDeleted = 1
	,[ModifiedDate] = GETDATE() 
	WHERE ID = @Id
END
GO



--==========================================================================================================================================
--==========================================================================================================================================
--==========================================================================================================================================
--==========================================================================================================================================
