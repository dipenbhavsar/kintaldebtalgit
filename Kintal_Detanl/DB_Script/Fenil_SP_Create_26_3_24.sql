--===========================================================================================================================================
--CREATE TABLE [dbo].[PatientTests], [PatientDrugs]
--===========================================================================================================================================

CREATE TABLE [dbo].[PatientDrugs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NULL,
	[TreatmentId] [int] NULL,
	[DrugId] [int] NULL,
	[Dosage] [nvarchar](128) NULL,
	[DrugType] [nvarchar](256) NULL,
	[Frequency] [nvarchar](256) NULL,
	[Notes] [nvarchar](1024) NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_PatientDrugs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PatientDrugs] ADD  CONSTRAINT [DF_PatientDrugs_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO

--===========================================================================================================================================
--===========================================================================================================================================


CREATE TABLE [dbo].[PatientTests](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NULL,
	[TreatmentId] [int] NULL,
	[TestId] [int] NULL,
	[TestDate] [datetime] NULL,
	[TestResult] [nvarchar](256) NULL,
	[Notes] [nvarchar](1024) NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_PatientTests] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PatientTests] ADD  CONSTRAINT [DF_PatientTests_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO

--===========================================================================================================================================
-- @PROCEDURES
--===========================================================================================================================================
--PROCEDURE [dbo].[wp_sp_InsertUpdatePatientDrugs]
--===========================================================================================================================================

DROP PROCEDURE [dbo].[wp_sp_InsertUpdatePatientDrugs]
GO

CREATE PROCEDURE [dbo].[wp_sp_InsertUpdatePatientDrugs]
@Id int = 0,
@PatientId INT,
@TreatmentId INT,
@DrugId INT = NULL,
@Dosage NVARCHAR(128) = NULL,
@DrugType NVARCHAR(256) = NULL,
@Frequency NVARCHAR(256) = NULL,
@Notes NVARCHAR(1024) = NULL,
@IsDeleted BIT = 0,
@ReturnValue INT OUTPUT
AS
BEGIN
	BEGIN TRY	
	IF(@Id > 0)
		BEGIN	
            UPDATE PatientDrugs
			SET 
				DrugId = @DrugId,
				Dosage = @Dosage,
				DrugType = @DrugType,
				Frequency = @Frequency,
				Notes = @Notes,
				IsDeleted = 0
            WHERE Id = @Id

            SELECT @ReturnValue = @Id
    END
    ELSE 
	BEGIN
			INSERT INTO PatientDrugs (PatientId, TreatmentId, DrugId, Dosage, DrugType, Frequency, Notes, IsDeleted)
			VALUES (@PatientId, @TreatmentId, @DrugId, @Dosage, @DrugType, @Frequency, @Notes, 0);
            SELECT @ReturnValue = SCOPE_IDENTITY()
	END
	
	END TRY
BEGIN CATCH		
	

	--RAISERROR with severity 11-19 will cause execution to jump to the CATCH block
	DECLARE @Msg NVARCHAR(MAX)  
	SELECT @Msg=ERROR_MESSAGE() 
	RAISERROR('%s', 18, 1,@Msg)
	
	SET XACT_ABORT OFF
END CATCH

END
GO

--===========================================================================================================================================
--PROCEDURE [dbo].[wp_sp_InsertUpdatePatientTests]
--===========================================================================================================================================
DROP PROCEDURE [dbo].[wp_sp_InsertUpdatePatientTests]
GO

CREATE PROCEDURE [dbo].[wp_sp_InsertUpdatePatientTests]
@Id int = 0
,@PatientId INT
,@TreatmentId INT
,@TestId INT = NULL
,@TestDate DATETIME = NULL
,@TestResult NVARCHAR(256) = NULL
,@Notes NVARCHAR(1024) = NULL
,@IsDeleted BIT = 0
,@ReturnValue INT OUTPUT
AS
BEGIN
	BEGIN TRY	
	IF(@Id > 0)
		BEGIN	
            UPDATE PatientTests
			SET 
				TestId = @TestId,
				TestDate = @TestDate,
				TestResult = @TestResult,
				Notes = @Notes,
				IsDeleted = 0	
            WHERE Id = @Id

            SELECT @ReturnValue = @Id
    END
    ELSE 
	BEGIN

			INSERT INTO PatientTests (PatientId, TreatmentId, TestId, TestDate, TestResult, Notes, IsDeleted)
			VALUES (@PatientId, @TreatmentId, @TestId, @TestDate, @TestResult, @Notes, 0);
            SELECT @ReturnValue = SCOPE_IDENTITY()
	END
	
	END TRY
BEGIN CATCH		
	

	--RAISERROR with severity 11-19 will cause execution to jump to the CATCH block
	DECLARE @Msg NVARCHAR(MAX)  
	SELECT @Msg=ERROR_MESSAGE() 
	RAISERROR('%s', 18, 1,@Msg)
	
	SET XACT_ABORT OFF
END CATCH

END
GO


--===========================================================================================================================================
--PROCEDURE [dbo].[wp_sp_getPatientTestsByTreatmentId]
--===========================================================================================================================================


DROP PROCEDURE [dbo].[wp_sp_getPatientTestsByTreatmentId]
GO

CREATE PROCEDURE [dbo].[wp_sp_getPatientTestsByTreatmentId]
@TreatmentId int
AS
BEGIN
	SELECT pt.*,T.TestName FROM PatientTests pt 
	LEFT JOIN TestMaster t ON PT.TestId = T.Id AND T.IsDeleted=0
	WHERE pt.TreatmentId = @TreatmentId and pt.IsDeleted = 0
END
GO


--===========================================================================================================================================
--PROCEDURE [dbo].[wp_sp_getPatientDrugsByTreatmentId]
--===========================================================================================================================================


DROP PROCEDURE [dbo].[wp_sp_getPatientDrugsByTreatmentId]
GO

CREATE PROCEDURE [dbo].[wp_sp_getPatientDrugsByTreatmentId]
@TreatmentId int
AS
BEGIN
	SELECT pd.*,d.DrugName FROM PatientDrugs pd 
	left join DrugMaster d on pd.DrugId = d.Id and d.IsDeleted=0
	where pd.TreatmentId = @TreatmentId and pd.IsDeleted = 0
END
GO


--===========================================================================================================================================
-- PROCEDURE [dbo].[wp_sp_DeletePatientDrugsById]
--===========================================================================================================================================

DROP PROCEDURE [dbo].[wp_sp_DeletePatientDrugsById]
GO

CREATE PROCEDURE [dbo].[wp_sp_DeletePatientDrugsById]
@Id INT
AS
BEGIN
	
	UPDATE PatientDrugs
	SET IsDeleted = 1
	WHERE ID = @Id
END
GO


--===========================================================================================================================================
--PROCEDURE [dbo].[wp_sp_DeletePatientTestsById]
--===========================================================================================================================================
DROP PROCEDURE [dbo].[wp_sp_DeletePatientTestsById]
GO

CREATE PROCEDURE [dbo].[wp_sp_DeletePatientTestsById]
@Id INT
AS
BEGIN
	
	UPDATE PatientTests
	SET IsDeleted = 1
	WHERE ID = @Id
END
GO

--===========================================================================================================================================
--===========================================================================================================================================
--===========================================================================================================================================
--===========================================================================================================================================
