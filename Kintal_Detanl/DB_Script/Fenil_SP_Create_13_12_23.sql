--===========================================================================================================================================
--ALTER TABLE [dbo].[Patient]
--===========================================================================================================================================

ALTER TABLE [dbo].[Patient] ADD DOB DATE;
ALTER TABLE [dbo].[Patient] ADD MaritalStatus VARCHAR(128);
ALTER TABLE [dbo].[Patient] ADD Gender VARCHAR(128);
ALTER TABLE [dbo].[Patient] ADD BloodGroup VARCHAR(128);
ALTER TABLE [dbo].[Patient] ADD PatientHeight INT;
ALTER TABLE [dbo].[Patient] ADD PatientWeight DECIMAL(6,2);
ALTER TABLE [dbo].[Patient] ADD PatientHistory VARCHAR(1024);
ALTER TABLE [dbo].[Patient] ADD ProfileBinary VARBINARY(MAX);
ALTER TABLE [dbo].[Patient] ADD [FileName] VARCHAR(1024);
ALTER TABLE [dbo].[Patient] ADD CreatedById	int
ALTER TABLE [dbo].[Patient] ADD EntryDate	datetime
ALTER TABLE [dbo].[Patient] ADD ModifiedById	int
ALTER TABLE [dbo].[Patient] ADD ModifiedDate	datetime
ALTER TABLE [dbo].[Patient] ADD DiseasesName VARCHAR(128);
ALTER TABLE [dbo].[Patient] ADD DiseasesDetails VARCHAR(128);
ALTER TABLE [dbo].[Patient] ADD IsAnyAllergy BIT;
ALTER TABLE [dbo].[Patient] ADD AllergyDetails VARCHAR(128);

--===========================================================================================================================================
--PROCEDURE [dbo].[wp_sp_Patient_InsertUpdate]
--===========================================================================================================================================

DROP PROCEDURE [dbo].[wp_sp_Patient_InsertUpdate]
GO

CREATE PROCEDURE [dbo].[wp_sp_Patient_InsertUpdate]
(
	@Id int
	,@Name varchar(100)
    ,@Address varchar(1000) = null
    ,@MobileNo varchar(20)
    ,@Email varchar(512) = null
    ,@TokenNo varchar(50)
    ,@DOB date
    ,@MaritalStatus varchar(128) = null
    ,@Gender varchar(128)
    ,@BloodGroup varchar(128) = null
    ,@PatientHeight int = null
    ,@PatientWeight decimal(6,2) = null
    ,@PatientHistory varchar(1024) = null
    ,@ProfileBinary varbinary(max) = null
	,@FileName varchar(1024) = null
	,@DiseasesName VARCHAR(128) = null
	,@DiseasesDetails VARCHAR(128) = null
	,@IsAnyAllergy BIT = 0
	,@AllergyDetails VARCHAR(128) = null
)
AS
BEGIN
	IF @Id IS NOT NULL AND @Id > 0
		BEGIN
			UPDATE [dbo].[Patient]
				SET [Name] = @Name
					,[Address] = @Address
					,[MobileNo] = @MobileNo
					,[Email] = @Email
					--,[TokenNo] = @TokenNo
					,[DOB] = @DOB
					,[MaritalStatus] = @MaritalStatus
					,[Gender] = @Gender
					,[BloodGroup] = @BloodGroup
					,[PatientHeight] = @PatientHeight
					,[PatientWeight] = @PatientWeight
					,[PatientHistory] = @PatientHistory
					,[ModifiedDate] = GETDATE()
					,[DiseasesName] = @DiseasesName
					,[DiseasesDetails] = @DiseasesDetails
					,[IsAnyAllergy] = @IsAnyAllergy
					,[AllergyDetails] = @AllergyDetails 
					--,[ProfileBinary] = @ProfileBinary
			WHERE Id = @Id

			/****** update profile if it changed ******/
			IF(ISNULL(DATALENGTH(@ProfileBinary),0) > 0)
			BEGIN
			UPDATE [dbo].[Patient]
			   SET [FileName] = @FileName,[ProfileBinary] = @ProfileBinary,[ModifiedDate] = GETDATE() WHERE [Id] = @Id
			END
			/****** end ******/

		END
	ELSE
		BEGIN
			DECLARE @MaxId INT
			SELECT @MaxId = MAX(Id) FROM Patient
			SET @MaxId = @MaxId + 1
			SET @TokenNo = @TokenNo + '-' + CAST(@MaxId AS VARCHAR)
			INSERT INTO [dbo].[Patient]
			       ([Name]
				   ,[Address]
				   ,[MobileNo]
				   ,[Email]
				   ,[TokenNo]
				   ,[DOB]
				   ,[MaritalStatus]
				   ,[Gender]
				   ,[BloodGroup]
				   ,[PatientHeight]
				   ,[PatientWeight]
				   ,[PatientHistory]
				   ,[ProfileBinary]
				   ,[FileName]
				   ,[EntryDate]
				   ,[ModifiedDate]
					,DiseasesName
					,DiseasesDetails
					,IsAnyAllergy
					,AllergyDetails )
			 VALUES
			       (@Name
				   ,@Address
				   ,@MobileNo
				   ,@Email
				   ,@TokenNo
				   ,@DOB
				   ,@MaritalStatus
				   ,@Gender
				   ,@BloodGroup
				   ,@PatientHeight
				   ,@PatientWeight
				   ,@PatientHistory
				   ,@ProfileBinary
				   ,@FileName
				   ,GETDATE()
				   ,GETDATE()
					,@DiseasesName
					,@DiseasesDetails
					,@IsAnyAllergy
					,@AllergyDetails )

			SET @Id = SCOPE_IDENTITY()
		END

		SELECT @Id AS ID, @TokenNo AS TokenNo
END
GO
--===========================================================================================================================================
--PROCEDURE [dbo].[wp_sp_SearchPatientData]
--===========================================================================================================================================
DROP PROCEDURE [dbo].[wp_sp_SearchPatientData]
GO
CREATE PROCEDURE [dbo].[wp_sp_SearchPatientData]
(
	@SearchText Varchar(512),
	@SelectMode int
)
AS
BEGIN
	
		IF @SelectMode = 0
			BEGIN
				IF @SearchText IS NOT NULL AND @SearchText <> ''
					BEGIN
						SELECT [Id],[Name],[Address],[MobileNo],[Email],[TokenNo],[DOB],[MaritalStatus],[Gender],[BloodGroup],[PatientHeight],[PatientWeight],[PatientHistory],IIF(ISNULL(DATALENGTH(ProfileBinary),0)=0,0,1) IsFileUploaded,DiseasesName,DiseasesDetails,IsAnyAllergy,AllergyDetails
						FROM [dbo].[Patient] 
						WHERE Name LIKE '%' + @SearchText + '%' 
						OR Email LIKE '%' + @SearchText + '%' 
						OR MobileNo LIKE '%' + @SearchText + '%'															  
					END
			END
		ELSE 
			BEGIN
				SELECT [Id],[Name],[Address],[MobileNo],[Email],[TokenNo],[DOB],[MaritalStatus],[Gender],[BloodGroup],[PatientHeight],[PatientWeight],[PatientHistory],IIF(ISNULL(DATALENGTH(ProfileBinary),0)=0,0,1) IsFileUploaded,DiseasesName,DiseasesDetails,IsAnyAllergy,AllergyDetails FROM [dbo].[Patient]
			END

END

GO
--===========================================================================================================================================
--PROCEDURE [dbo].[wp_sp_GetPatientDataById]
--===========================================================================================================================================
DROP PROCEDURE [dbo].[wp_sp_GetPatientDataById]
GO
CREATE PROCEDURE [dbo].[wp_sp_GetPatientDataById]
(	
	@Id int
)
AS
BEGIN	
		
		SELECT * FROM Patient WHERE Id = @Id

END

GO


--===========================================================================================================================================
--===========================================================================================================================================
--===========================================================================================================================================
--===========================================================================================================================================
