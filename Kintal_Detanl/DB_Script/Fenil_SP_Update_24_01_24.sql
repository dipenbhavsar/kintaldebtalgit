--==========================================================================================================================================
--@ VIEW [dbo].[wp_vw_PatientMain]
--==========================================================================================================================================


DROP VIEW [dbo].[wp_vw_PatientMain]
GO

CREATE VIEW [dbo].[wp_vw_PatientMain]
AS

SELECT [Id],[Name],[Address],[MobileNo],[Email],[TokenNo],[DOB],[MaritalStatus],[Gender],[BloodGroup],[PatientHeight],[PatientWeight],[PatientHistory],IIF(ISNULL(DATALENGTH(ProfileBinary),0)=0,0,1) IsFileUploaded,DiseasesName,DiseasesDetails,IsAnyAllergy,AllergyDetails FROM Patient

GO

--==========================================================================================================================================
--@ PROCEDURE [dbo].[wp_sp_SearchPatientData]
--==========================================================================================================================================

DROP PROCEDURE [dbo].[wp_sp_SearchPatientData]
GO


CREATE PROCEDURE [dbo].[wp_sp_SearchPatientData]
(
	@SearchText Varchar(512),
	@SelectMode int,
	@FromDate DateTime = '',
	@ToDate DateTime = ''
)
AS
BEGIN
	
		IF @SelectMode = 0
			BEGIN				
				IF @SearchText IS NOT NULL AND @SearchText <> ''
					BEGIN
						IF((@FromDate IS NOT NULL AND @FromDate <> '') AND (@ToDate IS NOT NULL AND @ToDate <> ''))
							BEGIN
								
								SELECT * FROM wp_vw_PatientMain WHERE (Name LIKE '%' + @SearchText + '%' OR
															  Email LIKE '%' + @SearchText + '%' OR
															  MobileNo LIKE '%' + @SearchText + '%')
															  AND Id IN (
															  SELECT DISTINCT PatientId FROM wp_vw_TreatmentMain
															  WHERE TreatmentDate >= @FromDate AND 
															  TreatmentDate <= @ToDate) Order By Name

							END
						ELSE
							BEGIN
								SELECT * FROM wp_vw_PatientMain WHERE Name LIKE '%' + @SearchText + '%' OR
															  Email LIKE '%' + @SearchText + '%' OR
															  MobileNo LIKE '%' + @SearchText + '%' Order By Name
							END
					END
			END
		ELSE 
			BEGIN
				IF((@FromDate IS NOT NULL AND @FromDate <> '') AND (@ToDate IS NOT NULL AND @ToDate <> ''))
					BEGIN
						SELECT * FROM wp_vw_PatientMain
						WHERE Id IN ( SELECT DISTINCT PatientId FROM wp_vw_TreatmentMain
									  WHERE TreatmentDate >= @FromDate AND 
									  TreatmentDate <= @ToDate)
						 Order By Name
					END
				ELSE
					BEGIN
						SELECT * FROM wp_vw_PatientMain Order By Name
					END
			END

END
GO


--==========================================================================================================================================
--==========================================================================================================================================
--==========================================================================================================================================
--==========================================================================================================================================
