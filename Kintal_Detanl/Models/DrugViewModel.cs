﻿using Microsoft.Ajax.Utilities;
using MspCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Linq;

namespace Kintal_Detanl.Models
{
    public class DrugViewModel
    {
        #region Properties
        public long Id { get; set; }
        public string DrugName { get; set; }
        public string GenericName { get; set; }
        public string Description { get; set; }
        #endregion

        #region Drug data layer
        public static DrugViewModel InsertOrUpdateDrugDetails(string conString, DrugViewModel drugModel)
        {
            using (SqlConnection cn = new SqlConnection(conString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("wp_sp_InsertUpdateDrugMaster", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", SqlUtils.SetDbConformValueLong(drugModel.Id));
                cmd.Parameters.AddWithValue("@DrugName", SqlUtils.SetDbConformValueString(drugModel.DrugName));
                cmd.Parameters.AddWithValue("@GenericName", SqlUtils.SetDbConformValueString(drugModel.GenericName));
                cmd.Parameters.AddWithValue("@Description", SqlUtils.SetDbConformValueString(drugModel.Description));

                SqlParameter outputParamReturnedValue = cmd.Parameters.Add("@ReturnValue", SqlDbType.Int);
                outputParamReturnedValue.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                drugModel.Id = !string.IsNullOrWhiteSpace(cmd.Parameters["@ReturnValue"].Value.ToString()) ? Convert.ToInt32(cmd.Parameters["@ReturnValue"].Value) : 0;
                cn.Close();
                return drugModel;
            }
        }
        public static List<DrugViewModel> GetDrugs(string conString)
        {
            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(conString))
            {
                string strSql = "wp_sp_getDrugMasterList";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);
                List<DrugViewModel> res = BindDrugsListFromDataTable(dtblResult);
                return (res != null && res.Count > 0) ? res : new List<DrugViewModel>();
            }
        }
        public static DrugViewModel GetDrugById(string conString, long id)
        {
            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(conString))
            {
                string strSql = "wp_sp_getDrugMasterById";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", SqlUtils.SetDbConformValueLong(id));
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);
                DrugViewModel res = BindDrugFromDataTable(dtblResult);
                return (res != null && res.Id > 0) ? res : new DrugViewModel();
            }
        }
        public static bool DeleteDrugById(string conString, long id)
        {
            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(conString))
            {
                string strSql = "wp_sp_DeleteDrugMasterById";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", SqlUtils.SetDbConformValueLong(id));
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);
                DrugViewModel res = BindDrugFromDataTable(dtblResult);
                return (res != null && res.Id > 0) ? true : false;
            }
        }
        #endregion

        #region Data Access
        private static List<DrugViewModel> BindDrugsListFromDataTable(DataTable dtDrug)
        {
            List<DrugViewModel> drugList = null;

            if (dtDrug != null && dtDrug.Rows.Count > 0)
            {
                drugList = new List<DrugViewModel>();
                using (DataTableReader dataTableReader = new DataTableReader(dtDrug))
                {
                    while (dataTableReader.Read())
                    {
                        DrugViewModel drugModel = new DrugViewModel();
                        LoadDrugObjectDataFromDataTableReader(dataTableReader, drugModel);
                        drugList.Add(drugModel);
                    }
                }
            }

            return drugList;
        }
        private static DrugViewModel BindDrugFromDataTable(DataTable dtDrug)
        {
            DrugViewModel drug = null;
            if (dtDrug != null && dtDrug.Rows.Count > 0)
            {
                drug = new DrugViewModel();
                DataTableReader inputDataTableReader = SqlUtils.CreateReaderMoveToFirstRow(dtDrug);
                LoadDrugObjectDataFromDataTableReader(inputDataTableReader, drug);
            }

            return drug;
        }
        private static void LoadDrugObjectDataFromDataTableReader(DataTableReader inputDataTableReader, DrugViewModel drugModel)
        {
            if (inputDataTableReader != null && inputDataTableReader.HasRows)
            {
                drugModel.Id = SqlUtils.GetInt64("Id", inputDataTableReader);
                drugModel.DrugName = SqlUtils.GetString("DrugName", inputDataTableReader);
                drugModel.GenericName = SqlUtils.GetString("GenericName", inputDataTableReader);
                drugModel.Description = SqlUtils.GetString("Description", inputDataTableReader);
            }
        }
        #endregion
    }
}