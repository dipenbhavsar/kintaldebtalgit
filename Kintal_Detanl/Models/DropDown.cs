﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Kintal_Detanl.Models
{
  public class DropDown
  {
    #region "Properties"

    /// <summary>
    /// Gets or sets the text.
    /// </summary>
    /// <value>
    /// The text.
    /// </value>
    public string Text { get; set; }

    /// <summary>
    /// Gets or sets the value.
    /// </summary>
    /// <value>
    /// The value.
    /// </value>
    public string Value { get; set; }

    #endregion

    #region Public Methods
    /// <summary>
    /// Method to bind DropDown List from DataTable
    /// </summary>
    /// <param name="dtDropDownData">Data Table</param>
    /// <param name="textColumnName">DropDown's Text Column Field</param>
    /// <param name="valueColumnName">DropDown's Value Column Field</param>
    /// <param name="isSelectRequired">Default Select Item required or not</param>
    /// <returns></returns>
    public static List<DropDown> BindDropDownFromTable(DataTable dtDropDownData, string textColumnName, string valueColumnName, bool isSelectRequired = false)
    {
      List<DropDown> dropDownList = new List<DropDown>();
      if (dtDropDownData != null && dtDropDownData.Rows.Count > 0)
      {
        foreach (DataRow dr in dtDropDownData.Rows)
        {
          DropDown dropDown = new DropDown();
          dropDown.Value = Convert.ToString(dr[valueColumnName]);
          dropDown.Text = Convert.ToString(dr[textColumnName]);
          dropDownList.Add(dropDown);
        }
      }

      if (isSelectRequired)
      {
        dropDownList.Insert(0, new DropDown { Text = "-- Select --", Value = "0" });
      }

      return dropDownList;
    }

    #endregion
  }
}