﻿using MspCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Kintal_Detanl.Models
{
    public class LabViewModel
    {
        #region Properties
        public long Id { get; set; }
        public string LabName { get; set; }
        public string Description { get; set; }
        #endregion

        #region Methods
        public static List<DropDown> BindLabDropdownList(string constring)
        {
            DataTable dtLabs = GetLabListFromDB(constring);
            return DropDown.BindDropDownFromTable(dtLabs, "LabName", "Id");
        }

        private static DataTable GetLabListFromDB(string constring)
        {
            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(constring))
            {
                string strSql = "wp_sp_GetLabs";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);

                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);
            }

            return dtblResult;
        }
        #endregion

        #region Lab data layer
        public static LabViewModel InsertOrUpdateLabDetails(string conString, LabViewModel labModel)
        {
            using (SqlConnection cn = new SqlConnection(conString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("wp_sp_InsertUpdateLabMaster", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", SqlUtils.SetDbConformValueLong(labModel.Id));
                cmd.Parameters.AddWithValue("@LabName", SqlUtils.SetDbConformValueString(labModel.LabName));
                cmd.Parameters.AddWithValue("@Description", SqlUtils.SetDbConformValueString(labModel.Description));

                SqlParameter outputParamReturnedValue = cmd.Parameters.Add("@ReturnValue", SqlDbType.Int);
                outputParamReturnedValue.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                labModel.Id = !string.IsNullOrWhiteSpace(cmd.Parameters["@ReturnValue"].Value.ToString()) ? Convert.ToInt32(cmd.Parameters["@ReturnValue"].Value) : 0;
                cn.Close();
                return labModel;
            }
        }
        public static List<LabViewModel> GetLabs(string conString)
        {
            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(conString))
            {
                string strSql = "wp_sp_getLabMasterList";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);
                List<LabViewModel> res = BindLabsListFromDataTable(dtblResult);
                return (res != null && res.Count > 0) ? res : new List<LabViewModel>();
            }
        }
        public static LabViewModel GetLabById(string conString, long id)
        {
            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(conString))
            {
                string strSql = "wp_sp_getLabMasterById";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", SqlUtils.SetDbConformValueLong(id));
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);
                LabViewModel res = BindLabFromDataTable(dtblResult);
                return (res != null && res.Id > 0) ? res : new LabViewModel();
            }
        }
        public static bool DeleteLabById(string conString, long id)
        {
            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(conString))
            {
                string strSql = "wp_sp_DeleteLabMasterById";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", SqlUtils.SetDbConformValueLong(id));
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);
                LabViewModel res = BindLabFromDataTable(dtblResult);
                return (res != null && res.Id > 0) ? true : false;
            }
        }
        #endregion

        #region Data Access
        private static List<LabViewModel> BindLabsListFromDataTable(DataTable dtLab)
        {
            List<LabViewModel> labList = null;

            if (dtLab != null && dtLab.Rows.Count > 0)
            {
                labList = new List<LabViewModel>();
                using (DataTableReader dataTableReader = new DataTableReader(dtLab))
                {
                    while (dataTableReader.Read())
                    {
                        LabViewModel labModel = new LabViewModel();
                        LoadLabObjectDataFromDataTableReader(dataTableReader, labModel);
                        labList.Add(labModel);
                    }
                }
            }

            return labList;
        }
        private static LabViewModel BindLabFromDataTable(DataTable dtLab)
        {
            LabViewModel lab = null;
            if (dtLab != null && dtLab.Rows.Count > 0)
            {
                lab = new LabViewModel();
                DataTableReader inputDataTableReader = SqlUtils.CreateReaderMoveToFirstRow(dtLab);
                LoadLabObjectDataFromDataTableReader(inputDataTableReader, lab);
            }

            return lab;
        }
        private static void LoadLabObjectDataFromDataTableReader(DataTableReader inputDataTableReader, LabViewModel labModel)
        {
            if (inputDataTableReader != null && inputDataTableReader.HasRows)
            {
                labModel.Id = SqlUtils.GetInt64("Id", inputDataTableReader);
                labModel.LabName = SqlUtils.GetString("LabName", inputDataTableReader);
                labModel.Description = SqlUtils.GetString("Description", inputDataTableReader);
            }
        }
        #endregion
    }
}