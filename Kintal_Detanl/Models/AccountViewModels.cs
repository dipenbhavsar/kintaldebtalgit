﻿using Microsoft.Ajax.Utilities;
using MspCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Linq;

namespace Kintal_Detanl.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        public int SrNo { get; set; }

        public int Id { get; set; }

        [Display(Name = "Token No")]
        public string TokenNo { get; set; }

        [Required]
        [Display(Name = "Patient Name")]
        public string Name { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Mobile No")]
        public string Mobile { get; set; }

        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [AllowHtml]
        [Display(Name = "Treatment Details")]
        public string LastTreatment { get; set; }

        [Display(Name = "Treatment Charges")]
        public decimal Charges { get; set; }

        [Display(Name = "Date of birth")]
        public DateTime? DOB { get; set; }
        public string DOBString { get; set; }

        [Display(Name = "Marital Status")]
        public string MaritalStatus { get; set; }

        [Display(Name = "Gender")]
        public string Gender { get; set; }

        [Display(Name = "Blood Group")]
        public string BloodGroup { get; set; }

        [Display(Name = "Patient Height")]
        public int? PatientHeight { get; set; }

        [Display(Name = "Patient Weight")]
        public decimal? PatientWeight { get; set; }

        [Display(Name = "Patient History")]
        public string PatientHistory { get; set; }

        public byte[] ProfileBinary { get; set; }
        public string FileName { get; set; }
        public string ProfileBase64String { get; set; }
        public string DiseasesName { get; set; }
        public string DiseasesDetails { get; set; }
        public bool IsAnyAllergy { get; set; }
        public string AllergyDetails { get; set; }
        public TreatmentViewModel TreatmentViewModelData { get; set; }
        public PatientDrugsViewModel PatientDrugsViewModel { get; set; }
        public PatientTestsViewModel PatientTestsViewModel { get; set; }

        #region Methods

        public void SavePatientData(string constring)
        {
            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(constring))
            {
                //Update Query to insert into  the database      
                string strSql = "wp_sp_Patient_InsertUpdate";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", SqlUtils.SetDbConformValueInt(Id));
                cmd.Parameters.AddWithValue("@Name", SqlUtils.SetDbConformValueString(Name));
                cmd.Parameters.AddWithValue("@Address", SqlUtils.SetDbConformValueString(Address));
                cmd.Parameters.AddWithValue("@MobileNo", SqlUtils.SetDbConformValueString(Mobile));
                cmd.Parameters.AddWithValue("@Email", SqlUtils.SetDbConformValueString(Email));
                cmd.Parameters.AddWithValue("@DOB", SqlUtils.SetDbConformValueDateTime(DOB));
                cmd.Parameters.AddWithValue("@MaritalStatus", SqlUtils.SetDbConformValueString(MaritalStatus));
                cmd.Parameters.AddWithValue("@Gender", SqlUtils.SetDbConformValueString(Gender));
                cmd.Parameters.AddWithValue("@BloodGroup", SqlUtils.SetDbConformValueString(BloodGroup));
                cmd.Parameters.AddWithValue("@PatientHeight", SqlUtils.SetDbConformValueInt(PatientHeight));
                cmd.Parameters.AddWithValue("@PatientWeight", SqlUtils.SetDbConformValueDecimal(PatientWeight));
                cmd.Parameters.AddWithValue("@PatientHistory", SqlUtils.SetDbConformValueString(PatientHistory));
                cmd.Parameters.AddWithValue("@ProfileBinary", SqlUtils.SetDbConformValueByteArray(ProfileBinary));
                cmd.Parameters.AddWithValue("@FileName", SqlUtils.SetDbConformValueString(FileName));
                cmd.Parameters.AddWithValue("@TokenNo", SqlUtils.SetDbConformValueString(TokenNo));
                cmd.Parameters.AddWithValue("@DiseasesName", SqlUtils.SetDbConformValueString(DiseasesName));
                cmd.Parameters.AddWithValue("@DiseasesDetails", SqlUtils.SetDbConformValueString(DiseasesDetails));
                cmd.Parameters.AddWithValue("@IsAnyAllergy", SqlUtils.SetDbConformValueBool(IsAnyAllergy));
                cmd.Parameters.AddWithValue("@AllergyDetails", SqlUtils.SetDbConformValueString(AllergyDetails));
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);

                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);

                if (dtblResult != null && dtblResult.Rows.Count > 0)
                {
                    Id = Convert.ToInt32(dtblResult.Rows[0][0]);
                    TokenNo = Convert.ToString(dtblResult.Rows[0][1]);
                }
            }

            //if (!string.IsNullOrWhiteSpace(TreatmentViewModelData.DoctorName))
            //{
            //    TreatmentViewModelData.PatientId = Id;
            //    SaveTreatmentData(constring, treatmentViewMoldel: TreatmentViewModelData);
            //}
        }

        public TreatmentViewModel SaveTreatmentData(string constring, TreatmentViewModel treatmentViewMoldel)
        {
            int id = 0;
            string tokenNo = string.Empty;

            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(constring))
            {
                //Update Query to insert into  the database      
                string strSql = "wp_sp_TreatmentMain_InsertUpdate";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", SqlUtils.SetDbConformValueInt(treatmentViewMoldel.Id));
                cmd.Parameters.AddWithValue("@PatientId", SqlUtils.SetDbConformValueInt(treatmentViewMoldel.PatientId));
                cmd.Parameters.AddWithValue("@DoctorName", SqlUtils.SetDbConformValueString(treatmentViewMoldel.DoctorName));
                cmd.Parameters.AddWithValue("@LabId", SqlUtils.SetDbConformValueInt(treatmentViewMoldel.LabId));
                cmd.Parameters.AddWithValue("@AppointmentDate", SqlUtils.SetDbConformValueDateTime(treatmentViewMoldel.AppointmentDate));
                cmd.Parameters.AddWithValue("@TreatmentDate", SqlUtils.SetDbConformValueDateTime(treatmentViewMoldel.TreatmentDate));
                cmd.Parameters.AddWithValue("@Treatment", SqlUtils.SetDbConformValueString(treatmentViewMoldel.Treatment));
                cmd.Parameters.AddWithValue("@Charges", SqlUtils.SetDbConformValueDecimal(treatmentViewMoldel.Charges));
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);

                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);

                if (dtblResult != null && dtblResult.Rows.Count > 0)
                {
                    id = Convert.ToInt32(dtblResult.Rows[0][0]);
                }
            }
            treatmentViewMoldel.Id = id;
            return treatmentViewMoldel;
        }

        public static List<RegisterViewModel> GetPatientListFromDB(string constring, string searchText, int selectMode, DateTime? fromDate, DateTime? toDate)
        {
            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(constring))
            {
                string strSql = "wp_sp_SearchPatientData";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SearchText", searchText);
                cmd.Parameters.AddWithValue("@SelectMode", selectMode);
                cmd.Parameters.AddWithValue("@FromDate", fromDate);
                cmd.Parameters.AddWithValue("@ToDate", toDate);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);
                List<RegisterViewModel> res = BindPatientListFromDataTable(dtblResult);
                return (res != null && res.Count > 0) ? res : new List<RegisterViewModel>();
            }
        }

        public static RegisterViewModel GetPatientDetailsByIdFromDB(string constring, int id)
        {
            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(constring))
            {
                string strSql = "wp_sp_GetPatientDataById";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", id);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);
                RegisterViewModel res = BindPatientSingleObjectFromDataTable(dtblResult);
                return res;
            }
        }

        public static void DeletePatientDetailsByIdFromDB(string constring, int id)
        {
            using (SqlConnection cn = new SqlConnection(constring))
            {
                string strSql = "wp_sp_DeletePatientDataById";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", id);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);

                cn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public static DataTable CheckClientRegisterDeviceInformation(string constring, string latitude, string longitutde, string isp)
        {
            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(constring))
            {
                string strSql = "wp_sp_CheckClientRegisterDeviceInfo";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@longitudes", longitutde);
                cmd.Parameters.AddWithValue("@latitudes", latitude);
                cmd.Parameters.AddWithValue("@isp", isp);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);

                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);
            }
            return dtblResult;
        }

        //public static List<RegisterViewModel> GetPatientList(string constring, string searchText, int selectMode, DateTime? fromDate, DateTime? toDate)
        //{
        //    List<RegisterViewModel> registerViewModelList = new List<RegisterViewModel>();

        //    DataTable dtPatientData = GetPatientListFromDB(constring, searchText, selectMode, fromDate, toDate);
        //    if (dtPatientData != null && dtPatientData.Rows.Count > 0)
        //    {
        //        int count = 1;
        //        foreach (DataRow drPatientData in dtPatientData.Rows)
        //        {
        //            registerViewModelList = new List<RegisterViewModel>();
        //            using (DataTableReader dataTableReader = new DataTableReader(dtPatientData))
        //            {
        //                while (dataTableReader.Read())
        //                {
        //                    RegisterViewModel registerViewModel = new RegisterViewModel();
        //                    registerViewModel.SrNo = count++;
        //                    registerViewModel.Id = SqlUtils.GetInt32("Id", dataTableReader);
        //                    registerViewModel.Name = SqlUtils.GetString("Name", dataTableReader);
        //                    registerViewModel.Address = SqlUtils.GetString("Address", dataTableReader);
        //                    registerViewModel.Mobile = SqlUtils.GetString("Mobile", dataTableReader);
        //                    registerViewModel.Email = SqlUtils.GetString("Email", dataTableReader);
        //                    registerViewModelList.Add(registerViewModel);
        //                }
        //            }
        //        }
        //    }

        //    return registerViewModelList;
        //}

        //public static RegisterViewModel GetPatientDetailsById(string constring, int id)
        //{
        //    RegisterViewModel registerViewModel = new RegisterViewModel();

        //    DataTable dtPatientData = GetPatientDetailsByIdFromDB(constring, id);
        //    if (dtPatientData != null && dtPatientData.Rows.Count > 0)
        //    {
        //        DataRow drPatientData = dtPatientData.Rows[0];
        //        registerViewModel.Id = Convert.ToInt32(drPatientData["Id"]);
        //        registerViewModel.TokenNo = Convert.ToString(drPatientData["TokenNo"]);
        //        registerViewModel.Name = Convert.ToString(drPatientData["Name"]);
        //        registerViewModel.Address = Convert.ToString(drPatientData["Address"]);
        //        registerViewModel.Mobile = Convert.ToString(drPatientData["MobileNo"]);
        //        registerViewModel.Email = Convert.ToString(drPatientData["Email"]);
        //    }

        //    return registerViewModel;
        //}

        #endregion

        #region patient data access
        private static void LoadPatientObjectDataFromDataTableReader(DataTableReader inputDataTableReader, RegisterViewModel patient)
        {
            if (inputDataTableReader != null && inputDataTableReader.HasRows)
            {
                //patient.SrNo = count++;
                patient.Id = SqlUtils.GetInt32("Id", inputDataTableReader);
                patient.Name = SqlUtils.GetString("Name", inputDataTableReader);
                patient.DOB = SqlUtils.GetDateTime("DOB", inputDataTableReader);
                patient.DOBString = patient.DOB.HasValue ? patient.DOB.Value.ToString("dd/MM/yyyy") : "";
                patient.Email = SqlUtils.GetString("Email", inputDataTableReader);
                patient.Address = SqlUtils.GetString("Address", inputDataTableReader);
                patient.Mobile = SqlUtils.GetString("MobileNo", inputDataTableReader);
                patient.MaritalStatus = SqlUtils.GetString("MaritalStatus", inputDataTableReader);
                patient.Gender = SqlUtils.GetString("Gender", inputDataTableReader);
                patient.BloodGroup = SqlUtils.GetString("BloodGroup", inputDataTableReader);
                patient.PatientHeight = SqlUtils.GetInt32("PatientHeight", inputDataTableReader);
                patient.PatientWeight = SqlUtils.GetDecimal("PatientWeight", inputDataTableReader);
                patient.PatientHistory = SqlUtils.GetString("PatientHistory", inputDataTableReader);
                patient.ProfileBinary = SqlUtils.GetByteArray("ProfileBinary", inputDataTableReader);
                patient.FileName = SqlUtils.GetString("FileName", inputDataTableReader);
                patient.ProfileBase64String = patient.ProfileBinary != null && patient.ProfileBinary.Length > 1 ? ("data:image/" + patient.FileName.Split('.')[1] + ";base64," + Convert.ToBase64String(patient.ProfileBinary)) : null;
                patient.DiseasesName = SqlUtils.GetString("DiseasesName", inputDataTableReader);
                patient.DiseasesDetails = SqlUtils.GetString("DiseasesDetails", inputDataTableReader);
                patient.IsAnyAllergy = SqlUtils.GetBoolean("IsAnyAllergy", inputDataTableReader);
                patient.AllergyDetails = SqlUtils.GetString("AllergyDetails", inputDataTableReader);
            }
        }
        public static RegisterViewModel BindPatientSingleObjectFromDataTable(DataTable dtPatient)
        {
            RegisterViewModel patient = null;
            if (dtPatient != null && dtPatient.Rows.Count > 0)
            {
                patient = new RegisterViewModel();
                DataTableReader inputDataTableReader = SqlUtils.CreateReaderMoveToFirstRow(dtPatient);
                LoadPatientObjectDataFromDataTableReader(inputDataTableReader, patient);
            }
            return patient;
        }
        public static List<RegisterViewModel> BindPatientListFromDataTable(DataTable dtPatient)
        {
            List<RegisterViewModel> patientList = null;
            if (dtPatient != null && dtPatient.Rows.Count > 0)
            {
                patientList = new List<RegisterViewModel>();
                using (DataTableReader dataTableReader = new DataTableReader(dtPatient))
                {
                    while (dataTableReader.Read())
                    {
                        RegisterViewModel patient = new RegisterViewModel();
                        LoadPatientObjectDataFromDataTableReader(dataTableReader, patient);
                        patientList.Add(patient);
                    }
                }
            }
            return patientList;
        }

        #endregion
    }
    public class TreatmentViewModel
    {
        #region  Properties

        public int Id { get; set; }
        public int PatientId { get; set; }
        [Required]
        [Display(Name = "Doctor Name:")]
        public string DoctorName { get; set; }
        [Required]
        [Display(Name = "Lab:")]
        public int LabId { get; set; }
        [Display(Name = "Appointment Date:")]
        public DateTime? AppointmentDate { get; set; }
        public string AppointmentDateString { get; set; }
        [Display(Name = "Treatment Date:")]
        public DateTime? TreatmentDate { get; set; }
        public string TreatmentDateString { get; set; }
        [Display(Name = "Treatment:")]
        public string Treatment { get; set; }
        [Display(Name = "Charges:")]
        public decimal Charges { get; set; }

        //to display lab name
        [Display(Name = "Lab Name:")]
        public string LabName { get; set; }

        #endregion


        #region Treatment data access
        private static void LoadTreatmentObjectDataFromDataTableReader(DataTableReader inputDataTableReader, TreatmentViewModel treatment)
        {
            if (inputDataTableReader != null && inputDataTableReader.HasRows)
            {
                //patient.SrNo = count++;
                treatment.Id = SqlUtils.GetInt32("Id", inputDataTableReader);
                treatment.Treatment = SqlUtils.GetString("Treatment", inputDataTableReader);
                treatment.TreatmentDate = SqlUtils.GetDateTime("TreatmentDate", inputDataTableReader);
                treatment.TreatmentDateString = treatment.TreatmentDate.HasValue ? treatment.TreatmentDate.Value.ToString("yyyy-MM-dd") : "";
                treatment.AppointmentDate = SqlUtils.GetDateTime("AppointmentDate", inputDataTableReader);
                treatment.AppointmentDateString = treatment.AppointmentDate.HasValue ? treatment.AppointmentDate.Value.ToString("yyyy-MM-dd") : "";
                treatment.DoctorName = SqlUtils.GetString("DoctorName", inputDataTableReader);
                treatment.Charges = SqlUtils.GetDecimal("Charges", inputDataTableReader);
                treatment.PatientId = SqlUtils.GetInt32("PatientId", inputDataTableReader);
                treatment.LabId = SqlUtils.GetInt32("LabId", inputDataTableReader);
                treatment.LabName = SqlUtils.GetString("LabName", inputDataTableReader);
            }
        }
        public static TreatmentViewModel BindTreatmentSingleObjectFromDataTable(DataTable dtTreatment)
        {
            TreatmentViewModel treatment = null;
            if (dtTreatment != null && dtTreatment.Rows.Count > 0)
            {
                treatment = new TreatmentViewModel();
                DataTableReader inputDataTableReader = SqlUtils.CreateReaderMoveToFirstRow(dtTreatment);
                LoadTreatmentObjectDataFromDataTableReader(inputDataTableReader, treatment);
            }
            return treatment;
        }
        public static List<TreatmentViewModel> BindTreatmentListFromDataTable(DataTable dtTreatment)
        {
            List<TreatmentViewModel> treatmentList = null;
            if (dtTreatment != null && dtTreatment.Rows.Count > 0)
            {
                treatmentList = new List<TreatmentViewModel>();
                using (DataTableReader dataTableReader = new DataTableReader(dtTreatment))
                {
                    while (dataTableReader.Read())
                    {
                        TreatmentViewModel treatment = new TreatmentViewModel();
                        LoadTreatmentObjectDataFromDataTableReader(dataTableReader, treatment);
                        treatmentList.Add(treatment);
                    }
                }
            }
            return treatmentList;
        }

        #endregion

        #region Treatment Methods

        //public static List<TreatmentViewModel> GetTreatmentListByPatientId(string constring, int patientId)
        //{
        //    List<TreatmentViewModel> treatmentViewModelList = new List<TreatmentViewModel>();

        //    DataTable dtTreatmentData = GetTreatmentListByPatientIdFromDB(constring, patientId);
        //    if (dtTreatmentData != null && dtTreatmentData.Rows.Count > 0)
        //    {
        //        foreach (DataRow drTreatmentData in dtTreatmentData.Rows)
        //        {
        //            TreatmentViewModel treatmentViewModel = new TreatmentViewModel();
        //            treatmentViewModel.Id = Convert.ToInt32(drTreatmentData["Id"]);
        //            treatmentViewModel.Treatment = Convert.ToString(drTreatmentData["Treatment"]);
        //            treatmentViewModel.TreatmentDate = !string.IsNullOrWhiteSpace(Convert.ToString(drTreatmentData["TreatmentDate"])) ? Convert.ToDateTime(drTreatmentData["TreatmentDate"]) : (DateTime?)null;
        //            treatmentViewModel.AppointmentDate = Convert.ToDateTime(drTreatmentData["AppointmentDate"]);
        //            treatmentViewModel.DoctorName = Convert.ToString(drTreatmentData["DoctorName"]);
        //            treatmentViewModel.Charges = Convert.ToDecimal(drTreatmentData["Charges"]);
        //            treatmentViewModel.PatientId = Convert.ToInt32(drTreatmentData["PatientId"]);
        //            treatmentViewModel.LabId = Convert.ToInt32(drTreatmentData["LabId"]);
        //            treatmentViewModel.LabName = !string.IsNullOrWhiteSpace(Convert.ToString(drTreatmentData["LabName"])) ? Convert.ToString(drTreatmentData["LabName"]) : "";
        //            treatmentViewModelList.Add(treatmentViewModel);
        //        }
        //    }

        //    return treatmentViewModelList;
        //}
        //public static TreatmentViewModel GetTreatmentMainObjectById(string constring, int id)
        //{
        //    TreatmentViewModel treatmentViewModel = new TreatmentViewModel();
        //    DataTable dtTreatmentData = GetTreatmentDataByIdFromDB(constring, id);
        //    if (dtTreatmentData != null && dtTreatmentData.Rows.Count > 0)
        //    {
        //        DataRow drTreatmentData = dtTreatmentData.Rows[0];
        //        treatmentViewModel.Id = Convert.ToInt32(drTreatmentData["Id"]);
        //        treatmentViewModel.Treatment = Convert.ToString(drTreatmentData["Treatment"]);
        //        treatmentViewModel.TreatmentDate = !string.IsNullOrWhiteSpace(Convert.ToString(drTreatmentData["TreatmentDate"])) ? Convert.ToDateTime(drTreatmentData["TreatmentDate"]) : (DateTime?)null;
        //        treatmentViewModel.AppointmentDate = Convert.ToDateTime(drTreatmentData["AppointmentDate"]);
        //        treatmentViewModel.DoctorName = Convert.ToString(drTreatmentData["DoctorName"]);
        //        treatmentViewModel.Charges = Convert.ToDecimal(drTreatmentData["Charges"]);
        //        treatmentViewModel.PatientId = Convert.ToInt32(drTreatmentData["PatientId"]);
        //        treatmentViewModel.LabId = Convert.ToInt32(drTreatmentData["LabId"]);
        //    }

        //    return treatmentViewModel;
        //}

        public static TreatmentViewModel GetTreatmentDataByIdFromDB(string constring, int id)
        {
            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(constring))
            {
                string strSql = "wp_sp_GetTreatmentDataById";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", id);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);
                TreatmentViewModel res = BindTreatmentSingleObjectFromDataTable(dtblResult);
                return res;
            }
        }

        public static List<TreatmentViewModel> GetTreatmentListByPatientIdFromDB(string conString, int patientId)
        {

            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(conString))
            {
                SqlCommand cmd = new SqlCommand("wp_sp_GetTreatmentDataByPatientId", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PatientId", patientId);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                //Fill the DataTable
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);

                List<TreatmentViewModel> res = BindTreatmentListFromDataTable(dtblResult);
                return res;
            }
        }

        public static void DeleteTreatmentDetailsByIdFromDB(string constring, int id)
        {
            using (SqlConnection cn = new SqlConnection(constring))
            {
                string strSql = "wp_sp_DeleteTreatmentDataById";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", id);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);

                cn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        #endregion
    }

    public class PatientDrugsViewModel
    {
        #region  Properties
        public int Id { get; set; }
        public int PatientId { get; set; }
        public int TreatmentId { get; set; }
        [Required]
        [Display(Name = "Select Drug")]
        public int DrugId { get; set; }
        public string DrugName { get; set; }
        public string Dosage { get; set; } = "";
        [Display(Name = "Select Type")]
        public string DrugType { get; set; } = "";
        public string Frequency { get; set; } = "";
        public string Notes { get; set; } = "";
        public bool IsDeleted { get; set; }= false;
        #endregion

        #region Patient drug data access
        private static void LoadPatientDrugsObjectDataFromDataTableReader(DataTableReader inputDataTableReader, PatientDrugsViewModel drug)
        {
            if (inputDataTableReader != null && inputDataTableReader.HasRows)
            {
                drug.Id = SqlUtils.GetInt32("Id", inputDataTableReader);
                drug.PatientId = SqlUtils.GetInt32("PatientId", inputDataTableReader);
                drug.TreatmentId = SqlUtils.GetInt32("TreatmentId", inputDataTableReader);
                drug.DrugId = SqlUtils.GetInt32("DrugId", inputDataTableReader);
                drug.DrugName = SqlUtils.GetString("DrugName", inputDataTableReader);
                drug.Dosage = SqlUtils.GetString("Dosage", inputDataTableReader);
                drug.DrugType = SqlUtils.GetString("DrugType", inputDataTableReader);
                drug.Frequency = SqlUtils.GetString("Frequency", inputDataTableReader);
                drug.Notes = SqlUtils.GetString("Notes", inputDataTableReader);
                drug.IsDeleted = SqlUtils.GetBoolean("IsDeleted", inputDataTableReader);
            }
        }
        public static List<PatientDrugsViewModel> BindPatientDrugsListFromDataTable(DataTable dtDrug)
        {
            List<PatientDrugsViewModel> drugs = null;
            if (dtDrug != null && dtDrug.Rows.Count > 0)
            {
                drugs = new List<PatientDrugsViewModel>();
                using (DataTableReader dataTableReader = new DataTableReader(dtDrug))
                {
                    while (dataTableReader.Read())
                    {
                        PatientDrugsViewModel drug = new PatientDrugsViewModel();
                        LoadPatientDrugsObjectDataFromDataTableReader(dataTableReader, drug);
                        drugs.Add(drug);
                    }
                }
            }
            return drugs;
        }
        #endregion

        #region Patient drug Methods
        public PatientDrugsViewModel CreateOrUpdatePatientDrugs(string conString, PatientDrugsViewModel drug)
        {
            using (SqlConnection cn = new SqlConnection(conString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("wp_sp_InsertUpdatePatientDrugs", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", SqlUtils.SetDbConformValueInt(drug.Id));
                cmd.Parameters.AddWithValue("@PatientId", SqlUtils.SetDbConformValueInt(drug.PatientId));
                cmd.Parameters.AddWithValue("@TreatmentId", SqlUtils.SetDbConformValueInt(drug.TreatmentId));
                cmd.Parameters.AddWithValue("@DrugId", SqlUtils.SetDbConformValueInt(drug.DrugId));
                cmd.Parameters.AddWithValue("@Dosage", SqlUtils.SetDbConformValueString(drug.Dosage));
                cmd.Parameters.AddWithValue("@DrugType", SqlUtils.SetDbConformValueString(drug.DrugType));
                cmd.Parameters.AddWithValue("@Frequency", SqlUtils.SetDbConformValueString(drug.Frequency));
                cmd.Parameters.AddWithValue("@Notes", SqlUtils.SetDbConformValueString(drug.Notes));
                cmd.Parameters.AddWithValue("@IsDeleted", SqlUtils.SetDbConformValueBool(drug.IsDeleted));

                SqlParameter outputParamReturnedValue = cmd.Parameters.Add("@ReturnValue", SqlDbType.Int);
                outputParamReturnedValue.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                drug.Id = !string.IsNullOrWhiteSpace(cmd.Parameters["@ReturnValue"].Value.ToString()) ? Convert.ToInt32(cmd.Parameters["@ReturnValue"].Value) : 0;
                cn.Close();
                return drug;
            }
        }
        public static List<PatientDrugsViewModel> GetPatientDrugsByTreatmentId(string constring, int treatmentId)
        {
            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(constring))
            {
                string strSql = "wp_sp_getPatientDrugsByTreatmentId";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TreatmentId", treatmentId);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);
                List<PatientDrugsViewModel> res = BindPatientDrugsListFromDataTable(dtblResult);
                return res;
            }
        }
        public static void DeletePatientDrugsById(string constring, int id)
        {
            using (SqlConnection cn = new SqlConnection(constring))
            {
                string strSql = "wp_sp_DeletePatientDrugsById";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", id);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                cn.Open();
                cmd.ExecuteNonQuery();
            }
        }
        #endregion
    }

    public class PatientTestsViewModel
    {
        #region  Properties
        public int Id { get; set; }
        public int PatientId { get; set; }
        public int TreatmentId { get; set; }
        [Required]
        [Display(Name = "Select Test")]
        public int TestId { get; set; }
        public string TestName { get; set; }
        [Display(Name = "Select Date")]
        public DateTime? TestDate { get; set; }
        public string TestDateString { get; set; } = "";
        [Display(Name = "Result")]
        public string TestResult { get; set; } = "";
        public string Notes { get; set; } = "";
        public bool IsDeleted { get; set; } = false;
        #endregion

        #region Patient test data access
        private static void LoadPatientTestsObjectDataFromDataTableReader(DataTableReader inputDataTableReader, PatientTestsViewModel test)
        {
            if (inputDataTableReader != null && inputDataTableReader.HasRows)
            {
                test.Id = SqlUtils.GetInt32("Id", inputDataTableReader);
                test.PatientId = SqlUtils.GetInt32("PatientId", inputDataTableReader);
                test.TreatmentId = SqlUtils.GetInt32("TreatmentId", inputDataTableReader);
                test.TestId = SqlUtils.GetInt32("TestId", inputDataTableReader);
                test.TestName = SqlUtils.GetString("TestName", inputDataTableReader);
                test.TestDate = SqlUtils.GetDateTime("TestDate", inputDataTableReader);
                test.TestDateString = test.TestDate.HasValue ? test.TestDate.Value.ToString("yyyy-MM-dd") : "";
                test.TestResult = SqlUtils.GetString("TestResult", inputDataTableReader);
                test.Notes = SqlUtils.GetString("Notes", inputDataTableReader);
                test.IsDeleted = SqlUtils.GetBoolean("IsDeleted", inputDataTableReader);
            }
        }
        public static List<PatientTestsViewModel> BindPatientTestsListFromDataTable(DataTable dtTest)
        {
            List<PatientTestsViewModel> tests = null;
            if (dtTest != null && dtTest.Rows.Count > 0)
            {
                tests = new List<PatientTestsViewModel>();
                using (DataTableReader dataTableReader = new DataTableReader(dtTest))
                {
                    while (dataTableReader.Read())
                    {
                        PatientTestsViewModel test = new PatientTestsViewModel();
                        LoadPatientTestsObjectDataFromDataTableReader(dataTableReader, test);
                        tests.Add(test);
                    }
                }
            }
            return tests;
        }
        #endregion

        #region Patient test Methods
        public PatientTestsViewModel CreateOrUpdatePatientTests(string conString, PatientTestsViewModel test)
        {
            using (SqlConnection cn = new SqlConnection(conString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("wp_sp_InsertUpdatePatientTests", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", SqlUtils.SetDbConformValueInt(test.Id));
                cmd.Parameters.AddWithValue("@PatientId", SqlUtils.SetDbConformValueInt(test.PatientId));
                cmd.Parameters.AddWithValue("@TreatmentId", SqlUtils.SetDbConformValueInt(test.TreatmentId));
                cmd.Parameters.AddWithValue("@TestId", SqlUtils.SetDbConformValueInt(test.TestId));
                cmd.Parameters.AddWithValue("@TestDate", SqlUtils.SetDbConformValueDateTime(test.TestDate));
                cmd.Parameters.AddWithValue("@TestResult", SqlUtils.SetDbConformValueString(test.TestResult));
                cmd.Parameters.AddWithValue("@Notes", SqlUtils.SetDbConformValueString(test.Notes));
                cmd.Parameters.AddWithValue("@IsDeleted", SqlUtils.SetDbConformValueBool(test.IsDeleted));

                SqlParameter outputParamReturnedValue = cmd.Parameters.Add("@ReturnValue", SqlDbType.Int);
                outputParamReturnedValue.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                test.Id = !string.IsNullOrWhiteSpace(cmd.Parameters["@ReturnValue"].Value.ToString()) ? Convert.ToInt32(cmd.Parameters["@ReturnValue"].Value) : 0;
                cn.Close();
                return test;
            }
        }
        public static List<PatientTestsViewModel> GetPatientTestsByTreatmentId(string constring, int treatmentId)
        {
            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(constring))
            {
                string strSql = "wp_sp_getPatientTestsByTreatmentId";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TreatmentId", treatmentId);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);
                List<PatientTestsViewModel> res = BindPatientTestsListFromDataTable(dtblResult);
                return res;
            }
        }
        public static void DeletePatientTestsById(string constring, int id)
        {
            using (SqlConnection cn = new SqlConnection(constring))
            {
                string strSql = "wp_sp_DeletePatientTestsById";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", id);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                cn.Open();
                cmd.ExecuteNonQuery();
            }
        }
        #endregion
    }
}
