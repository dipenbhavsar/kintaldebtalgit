﻿using Microsoft.Ajax.Utilities;
using MspCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Linq;

namespace Kintal_Detanl.Models
{
    public class TestViewModel
    {
        #region Properties
        public long Id { get; set; }
        public string TestName { get; set; }
        public string Description { get; set; }
        #endregion

        #region Test data layer
        public static TestViewModel InsertOrUpdateTestDetails(string conString, TestViewModel testModel)
        {
            using (SqlConnection cn = new SqlConnection(conString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("wp_sp_InsertUpdateTestMaster", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", SqlUtils.SetDbConformValueLong(testModel.Id));
                cmd.Parameters.AddWithValue("@TestName", SqlUtils.SetDbConformValueString(testModel.TestName));
                cmd.Parameters.AddWithValue("@Description", SqlUtils.SetDbConformValueString(testModel.Description));

                SqlParameter outputParamReturnedValue = cmd.Parameters.Add("@ReturnValue", SqlDbType.Int);
                outputParamReturnedValue.Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                testModel.Id = !string.IsNullOrWhiteSpace(cmd.Parameters["@ReturnValue"].Value.ToString()) ? Convert.ToInt32(cmd.Parameters["@ReturnValue"].Value) : 0;
                cn.Close();
                return testModel;
            }
        }
        public static List<TestViewModel> GetTests(string conString)
        {
            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(conString))
            {
                string strSql = "wp_sp_getTestMasterList";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);
                List<TestViewModel> res = BindTestsListFromDataTable(dtblResult);
                return (res != null && res.Count > 0) ? res : new List<TestViewModel>();
            }
        }
        public static TestViewModel GetTestById(string conString, long id)
        {
            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(conString))
            {
                string strSql = "wp_sp_getTestMasterById";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", SqlUtils.SetDbConformValueLong(id));
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);
                TestViewModel res = BindTestFromDataTable(dtblResult);
                return (res != null && res.Id > 0) ? res : new TestViewModel();
            }
        }
        public static bool DeleteTestById(string conString, long id)
        {
            DataTable dtblResult = null;
            using (SqlConnection cn = new SqlConnection(conString))
            {
                string strSql = "wp_sp_DeleteTestMasterById";
                SqlCommand cmd = new SqlCommand(strSql, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", SqlUtils.SetDbConformValueLong(id));
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                cn.Open();
                dtblResult = new DataTable();
                adpt.Fill(dtblResult);
                TestViewModel res = BindTestFromDataTable(dtblResult);
                return (res != null && res.Id > 0) ? true : false;
            }
        }
        #endregion

        #region Data Access
        private static List<TestViewModel> BindTestsListFromDataTable(DataTable dtTest)
        {
            List<TestViewModel> testList = null;

            if (dtTest != null && dtTest.Rows.Count > 0)
            {
                testList = new List<TestViewModel>();
                using (DataTableReader dataTableReader = new DataTableReader(dtTest))
                {
                    while (dataTableReader.Read())
                    {
                        TestViewModel testModel = new TestViewModel();
                        LoadTestObjectDataFromDataTableReader(dataTableReader, testModel);
                        testList.Add(testModel);
                    }
                }
            }

            return testList;
        }
        private static TestViewModel BindTestFromDataTable(DataTable dtTest)
        {
            TestViewModel test = null;
            if (dtTest != null && dtTest.Rows.Count > 0)
            {
                test = new TestViewModel();
                DataTableReader inputDataTableReader = SqlUtils.CreateReaderMoveToFirstRow(dtTest);
                LoadTestObjectDataFromDataTableReader(inputDataTableReader, test);
            }

            return test;
        }
        private static void LoadTestObjectDataFromDataTableReader(DataTableReader inputDataTableReader, TestViewModel testModel)
        {
            if (inputDataTableReader != null && inputDataTableReader.HasRows)
            {
                testModel.Id = SqlUtils.GetInt64("Id", inputDataTableReader);
                testModel.TestName = SqlUtils.GetString("TestName", inputDataTableReader);
                testModel.Description = SqlUtils.GetString("Description", inputDataTableReader);
            }
        }
        #endregion
    }
}