﻿using System.Web;
using System.Web.Optimization;

namespace Kintal_Detanl
{
  public class BundleConfig
  {
    // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
    public static void RegisterBundles(BundleCollection bundles)
    {
      bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                  "~/Scripts/jquery-{version}.js"));

      bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                  "~/Scripts/jquery.validate*"));

      // Use the development version of Modernizr to develop with and learn from. Then, when you're
      // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
      bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                  "~/Scripts/modernizr-*"));

      //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
      //          "~/Scripts/bootstrap.js",
      //          "~/Scripts/respond.js"));

      bundles.Add(new ScriptBundle("~/bundles/js").Include(
                "~/Scripts/jquery.min.js",
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/custom-tabs.js",
                //"~/Scripts/js/dropkick.js",
                "~/Scripts/icheck.min.js",
                "~/Scripts/moment.js",
                //"~/Scripts/bootstrap-datetimepicker.min.js",
                "~/Scripts/bootstrap-multiselect.js",
                "~/Scripts/custom.js",
                "~/Scripts/jquery.ui-jalert.js",
                "~/Scripts/Common.js",
                "~/Scripts/CommonModalPopups.js",
                "~/Scripts/jquery.cookie.js"));

      bundles.Add(new ScriptBundle("~/bundles/kendojs").Include(
                //"~/Scripts/kendo/2016.2.504/jquery.min.js",
                "~/Scripts/kendo/2016.2.504/kendo.all.min.js",
                "~/Scripts/kendo/2016.2.504/kendo.aspnetmvc.min.js"));

      bundles.Add(new StyleBundle("~/Content/kendouicss").Include(
                "~/Content/kendo/kendo.compatibility.css",
                "~/Content/kendo/2016.2.504/kendo.common-bootstrap.min.css",
                "~/Content/kendo/2016.2.504/kendo.default.min.css",
                "~/Content/kendo/2016.2.504/kendo.mobile.all.min.css",
                "~/Content/kendo/2016.2.504/kendo.dataviz.min.css",
                "~/Content/kendo/2016.2.504/kendo.bootstrap.min.css",
                "~/Content/kendo/2016.2.504/kendo.dataviz.bootstrap.min.css"));

      bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/css/fonts_googleapis.css",
                "~/Content/css/bootstrap.min.css",
                "~/Content/css/font-awesome.css",
                "~/Content/css/jquery.alerts.css",
                //"~/Content/css/bootstrap-datetimepicker.css",
                "~/Content/css/bootstrap-multiselect.css",
                "~/Content/css/style.css"));
    }
  }
}
