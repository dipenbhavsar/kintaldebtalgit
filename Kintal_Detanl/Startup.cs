﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Kintal_Detanl.Startup))]
namespace Kintal_Detanl
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
