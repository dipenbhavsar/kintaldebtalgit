﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Kintal_Detanl.Models;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Kintal_Detanl.Controllers
{
    public class TestController : Controller
    {
        #region Connection String Declarations

        private static readonly string _connStringDefault = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        #endregion

        // GET: Test/Index
        public ActionResult TestList()
        {
            return View();
        }

        [HttpPost]
        public JsonResult BindTestList([DataSourceRequest] DataSourceRequest request)
        {
            return Json(TestViewModel.GetTests(_connStringDefault).ToDataSourceResult(request));
        }

        //GET: Test/TestDetails/id
        public ActionResult TestDetails(int id = 0, string mode = "add")
        {
            ViewBag.PageMode = mode;
            var test = new TestViewModel();
            if (id > 0)
            {
                test = TestViewModel.GetTestById(_connStringDefault, id);
            }
            return View(test);
        }

        //POST: Test/TestDetails
        [HttpPost]
        public ActionResult TestDetails(TestViewModel testModel)
        {
            try
            {
                var result = new TestViewModel();
                if (testModel != null && !string.IsNullOrWhiteSpace(testModel.TestName))
                {
                    result = TestViewModel.InsertOrUpdateTestDetails(_connStringDefault.ToString(), testModel);
                }
                return Json(new object[] { true, result });
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, ex.Message });
            }
        }

        //DELETE: Test/id
        [HttpDelete]
        public JsonResult DeleteTest(int id)
        {
            try
            {
                TestViewModel.DeleteTestById(_connStringDefault, id);
                return Json(new object[] { true });
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, ex.Message });
            }
        }
    }
}