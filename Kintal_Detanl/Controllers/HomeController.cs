﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Kintal_Detanl.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Sockets;
using System.Web;
using System.Web.Mvc;

namespace Kintal_Detanl.Controllers
{
    public class HomeController : Controller
    {
        #region Connection String Declarations

        private static readonly string _connStringDefault = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        #endregion

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        #region Dashboard

        public ActionResult Dashboard()
        {
            return View();
        }

        #endregion

        #region Patient

        [HttpGet]
        public ActionResult PatientDetails(string id, string mode)
        {
            ViewBag.PageMode = mode;
            RegisterViewModel registerViewModel = !string.IsNullOrWhiteSpace(id) && Convert.ToInt32(id) > 0 ? RegisterViewModel.GetPatientDetailsByIdFromDB(_connStringDefault, Convert.ToInt32(id)) : new RegisterViewModel();
            registerViewModel.TreatmentViewModelData = new TreatmentViewModel();
            registerViewModel.PatientDrugsViewModel = new PatientDrugsViewModel();
            registerViewModel.PatientTestsViewModel = new PatientTestsViewModel();
            return View(registerViewModel);
        }

        [HttpPost]
        public JsonResult SavePatientDetails(HttpPostedFileBase httpPostedFileBase, string patientData)
        {
            try
            {
                RegisterViewModel registerViewModel = JsonConvert.DeserializeObject<RegisterViewModel>(patientData);
                //registerViewModel.TreatmentViewModelData = JsonConvert.DeserializeObject<TreatmentViewModel>(treatmentData);
                registerViewModel.TokenNo = string.IsNullOrWhiteSpace(registerViewModel.TokenNo) ? DateTime.Now.ToString("yyyyMMdd") : registerViewModel.TokenNo;
                if (httpPostedFileBase != null)
                {
                    using (Stream inputStream = httpPostedFileBase.InputStream)
                    {
                        MemoryStream memoryStream = inputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            inputStream.CopyTo(memoryStream);
                        }
                        registerViewModel.ProfileBinary = memoryStream.ToArray();
                        registerViewModel.FileName = httpPostedFileBase.FileName;
                    }
                }
                registerViewModel.SavePatientData(_connStringDefault);
                return Json(new object[] { true, registerViewModel.Id, registerViewModel.TokenNo });
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, ex.Message });
            }
        }

        [HttpGet]
        public ActionResult PatientList()
        {
            return View();
        }

        [HttpPost]
        public JsonResult BindPatientList([DataSourceRequest] DataSourceRequest request, string searchText = "", int selectMode = 0, string fromDate = "", string toDate = "")
        {
            DateTime? fromDate1 = null;
            DateTime? toDate1 = null;
            if (!string.IsNullOrWhiteSpace(fromDate))
            {
                fromDate1 = DateTime.ParseExact(fromDate, "dd/MM/yyyy", null);
            }

            if (!string.IsNullOrWhiteSpace(toDate))
            {
                toDate1 = DateTime.ParseExact(toDate, "dd/MM/yyyy", null);
            }

            List<RegisterViewModel> registerViewModelList = (string.IsNullOrWhiteSpace(searchText) && selectMode == 0) ? new List<RegisterViewModel>() : RegisterViewModel.GetPatientListFromDB(_connStringDefault, searchText, selectMode, fromDate1, toDate1);
            return Json(registerViewModelList.ToDataSourceResult(request));
        }

        [HttpPost]
        public JsonResult DeletePatient(int id)
        {
            try
            {
                RegisterViewModel.DeletePatientDetailsByIdFromDB(_connStringDefault, id);
                return Json(new object[] { true });
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, ex.Message });
            }
        }

        [HttpPost]
        public JsonResult BindLabDropDown()
        {
            List<DropDown> dropDownList = LabViewModel.BindLabDropdownList(_connStringDefault);
            return Json(dropDownList);
        }
        [HttpPost]
        public JsonResult BindDrugDropDown()
        {
            List<DrugViewModel> drugs = DrugViewModel.GetDrugs(_connStringDefault);
            List<DropDown> dropDownList = new List<DropDown>();
            if (drugs != null && drugs.Count > 0)
            {
                foreach (DrugViewModel dr in drugs)
                {
                    DropDown dropDown = new DropDown();
                    dropDown.Value = Convert.ToString(dr.Id);
                    dropDown.Text = Convert.ToString(dr.DrugName);
                    dropDownList.Add(dropDown);
                }
            }
            dropDownList.Insert(0, new DropDown { Text = "-- Select drug --", Value = "0" });
            return Json(dropDownList);
        }
        [HttpPost]
        public JsonResult BindTestDropDown()
        {
            List<TestViewModel> drugs = TestViewModel.GetTests(_connStringDefault);
            List<DropDown> dropDownList = new List<DropDown>();
            if (drugs != null && drugs.Count > 0)
            {
                foreach (TestViewModel dr in drugs)
                {
                    DropDown dropDown = new DropDown();
                    dropDown.Value = Convert.ToString(dr.Id);
                    dropDown.Text = Convert.ToString(dr.TestName);
                    dropDownList.Add(dropDown);
                }
            }
            dropDownList.Insert(0, new DropDown { Text = "-- Select test --", Value = "0" });
            return Json(dropDownList);
        }

        public JsonResult BindTreatmentList([DataSourceRequest] DataSourceRequest request, int patientId = 0)
        {
            List<TreatmentViewModel> treatmentViewModelList = TreatmentViewModel.GetTreatmentListByPatientIdFromDB(_connStringDefault, patientId);
            return Json(treatmentViewModelList != null && treatmentViewModelList.Count > 0 ? treatmentViewModelList.ToDataSourceResult(request) : null);
        }

        public JsonResult DeleteTreatmentDetailsById(int id)
        {
            try
            {
                TreatmentViewModel.DeleteTreatmentDetailsByIdFromDB(_connStringDefault, id);
                return Json(new object[] { true });
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, ex.Message });
            }
        }

        public JsonResult GetTreatmentDetailsById(int id)
        {
            try
            {
                TreatmentViewModel treatmentViewModel = TreatmentViewModel.GetTreatmentDataByIdFromDB(_connStringDefault, id);
                return Json(new object[] { true, treatmentViewModel });
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, ex.Message });
            }
        }

        [HttpPost]
        public JsonResult CheckClientRegisterDeviceInformation(decimal lat, decimal longi, string isp)
        {
            try
            {
                string latitudes = (Math.Truncate(lat)).ToString();
                string longitudes = (Math.Truncate(longi)).ToString();

                DataTable dataTable = RegisterViewModel.CheckClientRegisterDeviceInformation(_connStringDefault, latitudes, longitudes, isp);

                if (dataTable.Rows.Count > 0)
                {
                    return Json(new object[] { true });
                }
                else
                {
                    return Json(new object[] { false });
                }
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, ex.Message });
            }
        }

        [HttpGet]
        public ActionResult Error()
        {
            return View();
        }

        #endregion

        #region Send Email

        /// <summary>
        /// Method to send Email
        /// </summary>
        /// <param name="name"></param>
        /// <param name="contactNo"></param>
        /// <param name="email"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SendEmail(string name, string contactNo, string email, string message)
        {
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["Email"]);
                string subject = "Customer raised an Order Booking Enquiry";
                mailMessage.Subject = subject;
                mailMessage.IsBodyHtml = true;
                string mailBody = "Hello Eye Health Lab Team," + "<br/>One person has sent you request please find below details.<br/><br/>";
                mailBody += "<b>Name: </b> " + name;
                mailBody += "<br/><b>Contact No: </b> " + contactNo;
                mailBody += "<br/><b>Email Id: </b> " + email;
                mailBody += "<br/><b>Enquiry Details: </b> " + message;
                mailMessage.Body = mailBody;
                mailMessage.To.Add(ConfigurationManager.AppSettings["ToEmail"]);
                mailMessage.Priority = MailPriority.High;

                using (SmtpClient smtp = new SmtpClient())
                {
                    smtp.Host = ConfigurationManager.AppSettings["smtp"];
                    smtp.EnableSsl = false;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Timeout = 300000;
                    smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                    NetworkCredential networkCred = new NetworkCredential(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["Pass"]);
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = networkCred;
                    smtp.Send(mailMessage);
                }

                try
                {
                    mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["Email"]);
                    subject = "No Reply-Request Confirmation";
                    mailMessage.Subject = subject;
                    mailMessage.IsBodyHtml = true;
                    mailBody = "Dear " + name + "," + "<br/>Thank you for your interest, We have received your enquiry. We will get back to you soon. Thank you for choosing Eye Health Labs.<br/><br/>";
                    mailBody += "<b>Thanks & Regards, </b> ";
                    mailBody += "<br/><b>Eye Health Labs Team. </b> ";
                    mailBody += "<br/><b style='background-color: darkgrey;'>Please note this is unmonitored email address, Please do not send any message or email on this.</b> ";
                    mailMessage.Body = mailBody;
                    mailMessage.To.Add(email);
                    mailMessage.Priority = MailPriority.High;
                    using (SmtpClient smtp = new SmtpClient())
                    {
                        smtp.Host = ConfigurationManager.AppSettings["smtp"];
                        smtp.EnableSsl = false;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtp.Timeout = 300000;
                        smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                        NetworkCredential networkCred = new NetworkCredential(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["Pass"]);
                        smtp.UseDefaultCredentials = true;
                        smtp.Credentials = networkCred;
                        smtp.Send(mailMessage);
                    }
                }
                catch { }

                return Json(new object[] { true });
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, ex.InnerException.Message });
            }
        }

        #endregion


        #region patient treatment
        [HttpPost]
        public ActionResult SavedPatientTreatmentData(TreatmentViewModel treatmentData)
        {
            bool isSuccess = false;
            try
            {
                RegisterViewModel registerView = new RegisterViewModel();
                registerView.TreatmentViewModelData = registerView.SaveTreatmentData(_connStringDefault, treatmentData);
                if (registerView != null && registerView.TreatmentViewModelData != null && registerView.TreatmentViewModelData.Id > 0)
                {
                    isSuccess = true;
                }
                return Json(new object[] { isSuccess, registerView.TreatmentViewModelData });
            }
            catch (Exception ex)
            {
                return Json(new object[] { isSuccess, ex.Message });
            }
        }
        #endregion


        #region patient drug details

        public JsonResult BindPatientDrugList([DataSourceRequest] DataSourceRequest request, int treatmentId = 0)
        {
            List<PatientDrugsViewModel> patientDrugsViewModel = PatientDrugsViewModel.GetPatientDrugsByTreatmentId(_connStringDefault, treatmentId);
            var res =  patientDrugsViewModel != null && patientDrugsViewModel.Count > 0  && treatmentId > 0 ? patientDrugsViewModel : new List<PatientDrugsViewModel>();
            return Json(res.ToDataSourceResult(request));
        }
        [HttpPost]
        public ActionResult SavedPatientDrug(PatientDrugsViewModel drug)
        {
            bool isSuccess = false;
            try
            {
                if (drug != null && drug.PatientId > 0 && drug.TreatmentId > 0)
                {
                    PatientDrugsViewModel patientDrugsViewModel = drug;
                    drug = patientDrugsViewModel.CreateOrUpdatePatientDrugs(_connStringDefault, drug);
                    if (drug != null && drug.Id > 0)
                    {
                        isSuccess = true;
                    }
                }
                return Json(new object[] { isSuccess, drug });
            }
            catch (Exception ex)
            {
                return Json(new object[] { isSuccess, ex.Message });
            }
        }
        public JsonResult GetDrugDetailsById(int id, int treatmentId)
        {
            try
            {
                List<PatientDrugsViewModel> patientDrugs = PatientDrugsViewModel.GetPatientDrugsByTreatmentId(_connStringDefault, treatmentId);
                patientDrugs = patientDrugs.Where(x => x.Id == id).ToList();
                return Json(new object[] { true, patientDrugs.FirstOrDefault() });
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, ex.Message });
            }
        }
        public JsonResult DeleteDrugDetailsById(int id)
        {
            try
            {
                PatientDrugsViewModel.DeletePatientDrugsById(_connStringDefault, id);
                return Json(new object[] { true });
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, ex.Message });
            }
        }
        #endregion

        #region patient test details
        [HttpPost]
        public JsonResult BindPatientTestList([DataSourceRequest] DataSourceRequest request, int treatmentId = 0)
        {
            List<PatientTestsViewModel> patientTestsViewModel = PatientTestsViewModel.GetPatientTestsByTreatmentId(_connStringDefault, treatmentId);
            var res =  patientTestsViewModel != null && patientTestsViewModel.Count > 0  && treatmentId > 0 ? patientTestsViewModel : new List<PatientTestsViewModel>();
            return Json(res.ToDataSourceResult(request));
        }
        [HttpPost]
        public ActionResult SavedPatientTest(PatientTestsViewModel test)
        {
            bool isSuccess = false;
            try
            {
                if (test != null && test.PatientId > 0 && test.TreatmentId > 0)
                {
                    PatientTestsViewModel patientTestsViewModel = test;
                    test = patientTestsViewModel.CreateOrUpdatePatientTests(_connStringDefault, test);
                    if (test != null && test.Id > 0)
                    {
                        isSuccess = true;
                    }
                }
                return Json(new object[] { isSuccess, test });
            }
            catch (Exception ex)
            {
                return Json(new object[] { isSuccess, ex.Message });
            }
        }
        public JsonResult GetTestDetailsById(int id, int treatmentId)
        {
            try
            {
                List<PatientTestsViewModel> patientTests = PatientTestsViewModel.GetPatientTestsByTreatmentId(_connStringDefault, treatmentId);
                patientTests = patientTests.Where(x => x.Id == id).ToList();
                return Json(new object[] { true, patientTests.FirstOrDefault() });
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, ex.Message });
            }
        }
        public JsonResult DeleteTestDetailsById(int id)
        {
            try
            {
                PatientTestsViewModel.DeletePatientTestsById(_connStringDefault, id);
                return Json(new object[] { true });
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, ex.Message });
            }
        }

        #endregion
    }
}