﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Kintal_Detanl.Models;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Kintal_Detanl.Controllers
{
    public class LabController : Controller
    {
        #region Connection String Declarations

        private static readonly string _connStringDefault = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        #endregion

        // GET: Lab/Index
        public ActionResult LabList()
        {
            return View();
        }

        [HttpPost]
        public JsonResult BindLabList([DataSourceRequest] DataSourceRequest request)
        {
            return Json(LabViewModel.GetLabs(_connStringDefault).ToDataSourceResult(request));
        }

        //GET: Lab/LabDetails/id
        public ActionResult LabDetails(int id = 0, string mode = "add")
        {
            ViewBag.PageMode = mode;
            var lab = new LabViewModel();
            if (id > 0)
            {
                lab = LabViewModel.GetLabById(_connStringDefault, id);
            }
            return View(lab);
        }

        //POST: Lab/LabDetails
        [HttpPost]
        public ActionResult LabDetails(LabViewModel labModel)
        {
            try
            {
                var result = new LabViewModel();
                if (labModel != null && !string.IsNullOrWhiteSpace(labModel.LabName))
                {
                    result = LabViewModel.InsertOrUpdateLabDetails(_connStringDefault.ToString(), labModel);
                }
                return Json(new object[] { true, result });
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, ex.Message });
            }
        }

        //DELETE: Lab/id
        [HttpDelete]
        public JsonResult DeleteLab(int id)
        {
            try
            {
                LabViewModel.DeleteLabById(_connStringDefault, id);
                return Json(new object[] { true });
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, ex.Message });
            }
        }
    }
}