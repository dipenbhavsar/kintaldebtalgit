﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Kintal_Detanl.Models;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Kintal_Detanl.Controllers
{
    public class DrugController : Controller
    {
        #region Connection String Declarations

        private static readonly string _connStringDefault = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        #endregion

        // GET: Drug/Index
        public ActionResult DrugList()
        {
            return View();
        }

        [HttpPost]
        public JsonResult BindDrugList([DataSourceRequest] DataSourceRequest request)
        {
            return Json(DrugViewModel.GetDrugs(_connStringDefault).ToDataSourceResult(request));
        }

        //GET: Drug/DrugDetails/id
        public ActionResult DrugDetails(int id = 0, string mode = "add")
        {
            ViewBag.PageMode = mode;
            var drug = new DrugViewModel();
            if (id > 0)
            {
                drug = DrugViewModel.GetDrugById(_connStringDefault, id);
            }
            return View(drug);
        }

        //POST: Drug/DrugDetails
        [HttpPost]
        public ActionResult DrugDetails(DrugViewModel drugModel)
        {
            try
            {
                var result = new DrugViewModel();
                if (drugModel != null && !string.IsNullOrWhiteSpace(drugModel.DrugName))
                {
                    result = DrugViewModel.InsertOrUpdateDrugDetails(_connStringDefault.ToString(), drugModel);
                }
                return Json(new object[] { true, result });
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, ex.Message });
            }
        }

        //DELETE: Drug/id
        [HttpDelete]
        public JsonResult DeleteDrug(int id)
        {
            try
            {
                DrugViewModel.DeleteDrugById(_connStringDefault, id);
                return Json(new object[] { true });
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, ex.Message });
            }
        }
    }
}